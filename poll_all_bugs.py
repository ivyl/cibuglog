#!/usr/bin/env python3

from collections import defaultdict
from multiprocessing import Value
from multiprocessing.dummy import Pool
from datetime import timedelta
from django.utils import timezone

import django
import argparse
import sys
import re

django.setup()

from CIResults.models import BugTracker  # noqa

def poll_bug(bug):
    bug.poll()
    bug.save()

    with polled.get_lock():
        polled.value += 1
        print("{}/{}: polled {}".format(polled.value, len(bugs), bug.short_name))

bugtrackers = BugTracker.objects.all()

# Get the full list of bugs that are followed
bugs = list()
for bt in bugtrackers:
    if bt.components_followed_list is not None and bt.components_followed_since is not None:
        ids = bt.tracker.search_bugs_ids(components=bt.components_followed_list,
                                        created_since=bt.components_followed_since)
        bugs.extend(bt.get_or_create_bugs(ids))
        print("{}: Found {} bugs to poll".format(bt, len(ids)))
print()

# Do not poll again the bugs that have been polled recently
bugs = [bug for bug in bugs if bug.polled is None or timezone.now() - bug.polled > timedelta(days=1)]

# Start polling in parallel (10 threads) to speed up the polling
print("Polling {} outdated bugs needing to be polled".format(len(bugs)))
if len(bugs) > 0:
    polled = Value('i', 0)
    with Pool(processes=10) as pool:
        pool.map(poll_bug, bugs)
else:
    print("Found no bugs to poll. Did you set BugTracker.components_followed_since?")
