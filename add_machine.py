#!/usr/bin/env python3

import django
import argparse
import sys

django.setup()

from django.db import transaction
from CIResults.models import Machine, MachineTag  # noqa

def die(msg):
    print(msg, file=sys.stderr)
    sys.exit(1)

@transaction.atomic
def edit_machine(args):
    public = args.public=="yes"
    vetted = args.public=="yes"

    # Check if the alias machine exists
    if args.aliases is not None:
        try:
            alias_machine = Machine.objects.get(name=args.aliases)
        except Machine.DoesNotExist:
            die("The machine this machine is supposed to alias does not exist. Create it first...")
    else:
        alias_machine = None

    # List tags to show options
    tags = dict()
    for tag in MachineTag.objects.all():
        tags[tag.name] = tag

    # Find the list of tags that do not exist yet, and create them
    for tag_name in set(args.tags) - set(tags.keys()):
        tags[tag_name] = MachineTag.objects.create(name=tag_name, public=True)

    # Now get/create the machine
    try:
        machine = Machine.objects.get(name=args.name)
    except Machine.DoesNotExist:
        machine = Machine.objects.create(name=args.name, public=public)
        print("Created a new machine called '{}' (public={})".format(args.name, public))

    # Description
    if args.description is not None:
        machine.description = args.description
        print("Description set")

    # Public
    if args.public is not None:
        machine.public = public
        print("The machine is now {}".format("public" if public else "private"))

    # Vetted
    if args.vetted is not None:
        try:
            if vetted:
                machine.vet()
            else:
                machine.suppress()
            print("The machine is now {}".format("vetted" if vetted else "suppressed"))
        except ValueError as e:
            print(str(e))

    # Alias
    if alias_machine is not None:
        machine.aliases = alias_machine

    # Machine tags
    tags_to_add = set(args.tags) - set([mt.name for mt in machine.tags.all()])
    for tag_name in tags_to_add:
        machine.tags.add(tags[tag_name])
    if len(tags_to_add) > 0:
        print("Added {} tags: {}".format(len(tags_to_add), ", ".join(tags_to_add)))

    machine.save()


def show_status(args):
    try:
        m = Machine.objects.get(name=args.name)
    except Machine.DoesNotExist:
        die("The machine '{}' does not exist".format(args.name))

    print("{}:".format(m.name))
    print("    description: {}".format(m.description))
    print("    Vetted on  : {}".format(m.vetted_on))
    print("    Public     : {}".format(m.public))
    print("    Tags       : {}".format([t.name for t in m.tags.all()]))


def list_machines():
    for machine in Machine.objects.all():
        print(machine)


# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("-n", "--name", help="Name of the machine to add/edit", required=True)
parser.add_argument("-d", "--description", help="Description of the machine")
parser.add_argument("-t", "--tag", dest="tags", type=str, default=[], action='append',
                    help="Add a machine tag (create a public tag if it does not exist)")
parser.add_argument("-a", "--aliases", help="Is an alias of another machine")
parser.add_argument("--public", help="Should the machine be considered public?",
                    default=None, choices=('no', 'yes'))
parser.add_argument("--vetted", help="Should the machine be considered vetted?",
                    default=None, choices=('no', 'Yes'))
parser.add_argument("-s", "--status", action="store_true", help="Show the current status of the machine")
parser.add_argument("-l", "--list", action="store_true", help="List the machines")

args = parser.parse_args()

if args.status:
    show_status(args)
elif args.list:
    list_machines()
else:
    edit_machine(args)

sys.exit(0)
