"""cibuglog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework import routers
from django.contrib import admin
from django.conf import settings

from CIResults import views
from CIResults.models import Test, Machine

router = routers.DefaultRouter()
router.register(r'build', views.REST_BuildViewSet)
router.register(r'component', views.REST_ComponentViewSet)
router.register(r'issuefilter', views.REST_IssueFilterViewSet)
router.register(r'runconfig', views.REST_RunConfigViewSet)
router.register(r'test', views.REST_TestSet)
router.register(r'bugtrackeraccount', views.REST_BugTrackerAccountViewSet) # WARNING: Hard-coded URL in helpers.js

base_patterns = [
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/metrics', views.api_metrics),  # WARNING: counts private tests, machines and issues
    url(r'^api/', include((router.urls, "CIResults"), namespace='api')),

    url(r'^$', views.index, name='CIResults-index'),
    url(r'^public/index.html$', views.index, name='CIResults-index-public', kwargs={"public": True}),

    # Issues
    url(r'^issue/$', views.IssueListView.as_view(), name='CIResults-issues-list'),
    url(r'^issue/(?P<action>(create))', views.IssueView.as_view(), name='CIResults-issue'),
    url(r'^issue/(?P<pk>[0-9]+)/(?P<action>(edit|archive|restore|show|hide|history))$', views.IssueView.as_view(), name='CIResults-issue'),
    url(r'^issuefilterassoc/(?P<pk>[0-9]+)/(?P<action>(history))$', views.IssueFilterAssociatedView.as_view(), name='CIResults-issuefilterassoc'),
    url(r'^issuefilter/(?P<pk>[0-9]+)/(?P<action>(history))$', views.IssueFilterView.as_view(), name='CIResults-issuefilter'),
    url(r'^issuefilter/(?P<action>(stats))$', views.IssueFilterView.as_view(), name='CIResults-issuefilter'),
    url(r'^public/issue/index.html$', views.IssueListView.as_view(), name='CIResults-issues-list-public', kwargs={"public": True}),

    # Tests
    url(r'^test/$', views.TestListView.as_view(), name="CIResults-tests", kwargs={"public": False}),
    url(r'^test/(?P<pk>[0-9]+)/(?P<action>(history))$', views.TestView.as_view(), name='CIResults-test'),
    url(r'^test/(?P<pk>[0-9]+)/edit$', views.TestEditView.as_view(), name='CIResults-test-edit'),
    url(r'^test/(?P<pk>[0-9]+)/rename$', views.TestRenameView.as_view(), name='CIResults-test-rename'),
    url(r'^test/(?P<pk>[0-9]+)/suppress$', views.object_suppress, name='CIResults-test-suppress', kwargs={"klass": Test}),
    url(r'^test/(?P<pk>[0-9]+)/vet$', views.object_vet, name='CIResults-test-vet', kwargs={"klass": Test}),
    url(r'^test/(?P<action>(vet))$', views.TestView.as_view(), name='CIResults-test'),
    url(r'^tests/mass-rename$', views.TestMassRenameView.as_view(), name="CIResults-tests-massrename", kwargs={"public": False}),
    url(r'^public/tests.html$', views.TestListView.as_view(), name="CIResults-tests-public", kwargs={"public": True}),

    # Machines
    url(r'^machine/$', views.MachineListView.as_view(), name="CIResults-machines", kwargs={"public": False}),
    url(r'^machine/(?P<pk>[0-9]+)/edit$', views.MachineEditView.as_view(), name='CIResults-machine-edit'),
    url(r'^machine/(?P<pk>[0-9]+)/suppress$', views.object_suppress, name='CIResults-machine-suppress', kwargs={"klass": Machine}),
    url(r'^machine/(?P<pk>[0-9]+)/vet$', views.object_vet, name='CIResults-machine-vet', kwargs={"klass": Machine}),
    url(r'^machine/(?P<pk>[0-9]+)/(?P<action>(history))$', views.MachineView.as_view(), name='CIResults-machine'),
    url(r'^machine/(?P<action>(vet))$', views.MachineView.as_view(), name='CIResults-machine'),
    url(r'^public/machines.html$', views.MachineListView.as_view(), name="CIResults-machines-public", kwargs={"public": True}),

    # Results
    url(r'^results/all', views.TestResultListView.as_view(), name="CIResults-results"),
    url(r'^results/knownfailures$', views.KnownFailureListView.as_view(), name="CIResults-knownfailures"),

    # Histories
    url(r'^testsuite/(?P<pk>[0-9]+)/(?P<action>(history))$', views.TestSuiteView.as_view(), name='CIResults-testsuite'),
    url(r'^textstatus/(?P<pk>[0-9]+)/(?P<action>(history))$', views.TextStatusView.as_view(), name='CIResults-textstatus'),
    url(r'^textstatus/(?P<action>(vet))$', views.TextStatusView.as_view(), name='CIResults-textstatus'),
    url(r'^runcfg/(?P<pk>[0-9]+)/(?P<action>(history))$', views.RunConfigView.as_view(), name='CIResults-runcfg'),
    url(r'^component/(?P<pk>[0-9]+)/(?P<action>(history))$', views.ComponentView.as_view(), name='CIResults-component'),
    url(r'^build/(?P<pk>[0-9]+)/(?P<action>(history))$', views.BuildView.as_view(), name='CIResults-build'),
    url(r'^runcfgtag/(?P<pk>[0-9]+)/(?P<action>(history))$', views.RunConfigTagView.as_view(), name='CIResults-runcfgtag'),
    url(r'^testresult/(?P<pk>[0-9]+)/(?P<action>(history))$', views.TestResultView.as_view(), name='CIResults-testresult'),

    # Metrics
    url(r'^metrics/overview', views.metrics_overview, name='CIResults-metrics-overview'),
    url(r'^metrics/bugs', views.metrics_bugs, name='CIResults-metrics-bugs'),
    url(r'^metrics/open_bugs$', views.open_bugs, name="CIResults-metrics-open-bugs"),
    url(r'^metrics/passrate', views.metrics_passrate_history, name='CIResults-metrics-passrate'),
    url(r'^metrics/runtime', views.metrics_runtime_history, name='CIResults-metrics-runtime'),
    url(r'^public/metrics/overview.html$', views.metrics_overview, name='CIResults-metrics-overview-public',
        kwargs={"public": True}),
    url(r'^public/metrics/bugs.html$', views.metrics_bugs, name='CIResults-metrics-bugs-public',
        kwargs={"public": True}),
    url(r'^public/metrics/open_bugs.html$', views.open_bugs, name="CIResults-metrics-open-bugs-public",
        kwargs={"public": True}),
    url(r'^public/metrics/passrate.html$', views.metrics_passrate_history, name='CIResults-metrics-passrate-public',
        kwargs={"public": True}),
    url(r'^public/metrics/runtime.html$', views.metrics_runtime_history, name='CIResults-metrics-runtime-public',
        kwargs={"public": True}),
]

if settings.DEBUG:
    import debug_toolbar
    base_patterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + base_patterns

if settings.URL_PREFIX:
    urlpatterns = [url('^{}/'.format(settings.URL_PREFIX), include(base_patterns))]
else:
    urlpatterns = base_patterns
