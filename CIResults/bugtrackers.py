from django.utils.functional import cached_property
from django.db import IntegrityError
from django.utils import timezone

from dateutil import parser as dateparser
from jira import JIRA
import xmlrpc.client
import requests
import pytz

from .models import Person, BugComment, BugTrackerAccount


class BugTrackerCommon:
    def _can_create(self, bug):
        if bug.bug_id:
            raise ValueError("Bug already has a bug id assigned")
        if not bug.tracker.project:
            raise ValueError("No project defined for tracker")

    def __init__(self, db_bugtracker):
        self.db_bugtracker = db_bugtracker

    @staticmethod
    def join(a, b):
        return str(a).rstrip("/") + "/" + str(b).lstrip("/")

    @cached_property
    def accounts_cached(self):
        accounts = dict()
        for account in BugTrackerAccount.objects.filter(tracker=self.db_bugtracker):
            accounts[account.user_id] = account
        return accounts

    def find_or_create_account(self, user_id, full_name=None, email=None):
        account = self.accounts_cached.get(user_id)
        if account is None:
            person = Person.objects.create(full_name=full_name, email=email)
            account = BugTrackerAccount.objects.create(tracker=self.db_bugtracker, person=person, user_id=user_id,
                                                       is_developer=False)
            self.accounts_cached[account.user_id] = account
        else:
            # We found a match. Update the full name and email address, if it changed
            modified = False
            if full_name is not None and account.person.full_name != full_name:
                account.person.full_name = full_name
                modified = True
            if email is not None and account.person.email != email:
                account.person.email = email
                modified = True
            if modified:
                account.person.save()
        return account


class Untracked(BugTrackerCommon):
    def poll(self, bug):
        bug.title = "UNKNOWN"
        bug.status = "UNKNOWN"

    def search_bugs_ids(self, components=None, created_since=None, status=None):
        return set()

    def add_comment(self, bug, comment):
        # Nothing to do, just silently ignore
        pass

    def create_bug(self, bug):
        pass

    @property
    def open_statuses(self):
        return []


class Bugzilla(BugTrackerCommon):
    def __init__(self, db_bugtracker):
        super().__init__(db_bugtracker)

        self._proxy = xmlrpc.client.ServerProxy("{}/xmlrpc.cgi".format(self.db_bugtracker.url),
                                                use_builtin_types=True)

    @property
    def open_statuses(self):
        return ["NEW", "ASSIGNED", "REOPENED", "NEEDINFO"]

    def __get_user_id(self, bug, field):
        field_name = "{}_detail".format(field)
        email = bug.get(field_name, dict()).get('email')
        if email is not None:
            return email

        name = bug.get(field_name, dict()).get('name')
        if name is not None:
            return name

        raise ValueError("Cannot find a good identifier for the user of the bug {}".format(bug['id']))

    def __find_closure_date(self, bug_id):
        bugs_history = self._proxy.Bug.history({"ids": bug_id})['bugs']
        if len(bugs_history) == 1:
            history = bugs_history[0]['history']
            for update in reversed(history):
                for change in update['changes']:
                    if (change['field_name'] == 'status' and
                        change['added'] not in self.open_statuses and
                            change['removed'] in self.open_statuses):
                        return timezone.make_aware(update['when'], pytz.utc)
        return None

    def __list_to_str(self, bl):
        if type(bl) is list:
            return ",".join(bl)
        else:
            return bl

    def _bug_id_parser(self, bug):
        try:
            return int(bug.bug_id)
        except Exception as e:
            raise ValueError("Bugzilla's IDs should be integers ({})".format(bug.bug_id)) from e

    def poll(self, bug):
        bug_id = self._bug_id_parser(bug)

        # Query the ID
        bugs = self._proxy.Bug.get({"ids": bug_id})['bugs']
        if len(bugs) == 1:
            b = bugs[0]

            bug.title = b['summary']

            status = b['status']
            if len(b['resolution']) > 0:
                status += "/{}".format(b['resolution'])
            bug.status = status

            # Only get description if we haven't polled it before
            if bug.description is None:
                opts = {"ids": bug_id, "include_fields": ["text", "count"]}
                # Bug description is the first comment in Bugzilla bug
                comment = self._proxy.Bug.comments(opts)['bugs']["{}".format(bug_id)]['comments'][0]
                if int(comment['count']) != 0:
                    raise ValueError("Comment parsed for description is not "
                                     "first comment. Comment count: {}".format(comment['count']))
                bug.description = comment['text']

            bug.created = timezone.make_aware(b['creation_time'], pytz.utc)
            bug.updated = timezone.make_aware(b['last_change_time'], pytz.utc)

            # If the bug is closed and we don't know when it was, ask the history
            if not b['is_open'] and bug.closed is None:
                bug.closed = self.__find_closure_date(bug_id)
            elif b['is_open']:
                bug.closed = None

            bug.creator = self.find_or_create_account(self.__get_user_id(b, 'creator'),
                                                      email=b['creator_detail'].get('email'),
                                                      full_name=b['creator_detail']['real_name'])
            bug.assignee = self.find_or_create_account(self.__get_user_id(b, 'assigned_to'),
                                                       email=b['assigned_to_detail'].get('email'),
                                                       full_name=b['assigned_to_detail']['real_name'])
            bug.product = b['product']
            bug.component = b['component']
            bug.priority = b['priority']

            if self.db_bugtracker.features_field is not None and len(self.db_bugtracker.features_field) > 0:
                bug.features = self.__list_to_str(b[self.db_bugtracker.features_field])
            if self.db_bugtracker.platforms_field is not None and len(self.db_bugtracker.platforms_field) > 0:
                bug.platforms = self.__list_to_str(b[self.db_bugtracker.platforms_field])

            # Get the list of comments, if the bug is already saved in the database
            if bug.id is not None and bug.has_new_comments:
                opts = {"ids": bug_id, 'include_fields': ['id', 'creator', 'count', 'time']}
                if bug.comments_polled is not None:
                    opts["new_since"] = bug.comments_polled

                # Get the lists of comments and create objects in our DB
                now = timezone.now()
                for c in self._proxy.Bug.comments(opts)['bugs']["{}".format(bug_id)]['comments']:
                    try:
                        account = self.find_or_create_account(c['creator'], email=c['creator'])
                        url = "{}#c{}".format(bug.url, c['count'])
                        BugComment.objects.create(bug=bug, account=account,
                                                  comment_id=c['id'], url=url,
                                                  created_on=timezone.make_aware(c['time'], pytz.utc))
                    except IntegrityError:
                        # We may have already imported the comment
                        pass
                bug.comments_polled = now
        else:
            raise ValueError("Could not find the bug ID {} on {}".format(bug_id, self.name))

    def search_bugs_ids(self, components=None, created_since=None, status=None):
        query = {"include_fields": ['id']}

        if components is not None:
            query['component'] = components

        if created_since is not None:
            query['creation_time'] = created_since

        if status is not None:
            query['status'] = status

        return set([str(r['id']) for r in self._proxy.Bug.search(query)['bugs']])

    def get_auth_token(self):
        username = self.db_bugtracker.username
        password = self.db_bugtracker.password

        if username is None or len(username) == 0 or password is None or len(password) == 0:
            raise ValueError("Invalid credentials")

        ret = self._proxy.User.login({"login": username, "password": password, "restrict_login": True})
        return ret.get('token')

    def add_comment(self, bug, comment):
        token = self.get_auth_token()
        if token is None:
            raise ValueError("Authentication failed. Can't post a comment")

        bug_id = self._bug_id_parser(bug)
        self._proxy.Bug.add_comment({'token': token, 'id': bug_id, 'comment': str(comment)})

    def create_bug(self, bug):
        self._can_create(bug)

        summary = bug.title
        description = bug.description
        # FIXME: Components can have '/' in the name, so need to figure out
        # the proper way to split this. This will work, assuming the Product
        # doesn't have '/' in it
        project_key = bug.tracker.project
        product, component = project_key.split('/', 1)

        token = self.get_auth_token()
        if token is None:
            raise ValueError("Invalid credentials")

        new_bug = self._proxy.Bug.create(
                               {'token': token,
                                'summary': summary,
                                'description': description,
                                'product': product,
                                'component': component,
                                'version': "unspecified"  # This info is not stored in DB. Default to sane value
                                })

        # new bug ID will be used to create the bug in the database
        return new_bug["id"]


class Jira(BugTrackerCommon):
    def __init__(self, db_bugtracker):
        super().__init__(db_bugtracker)

    @cached_property
    def jira(self):
        jira_options = {
            'server': self.db_bugtracker.url,
            'verify': False
        }
        if len(self.db_bugtracker.username) > 0 and len(self.db_bugtracker.password) > 0:
            return JIRA(jira_options, basic_auth=(self.db_bugtracker.username,
                                                  self.db_bugtracker.password))
        else:
            return JIRA(jira_options)

    @property
    def open_statuses(self):
        return ['Open', 'In Progress']

    def poll(self, bug):
        # TODO: Implement the closed property

        issue = self.jira.issue(bug.bug_id)

        bug.title = issue.fields.summary
        bug.status = issue.fields.status.name
        bug.description = issue.fields.description

        if hasattr(issue.fields, "priority") and issue.fields.priority is not None:
            bug.priority = issue.fields.priority.name

        bug.created = dateparser.parse(issue.fields.created)
        bug.updated = dateparser.parse(issue.fields.updated)

        bug.creator = self.find_or_create_account(issue.fields.creator.key,
                                                  full_name=issue.fields.creator.displayName)
        if issue.fields.assignee is not None:
            bug.assignee = self.find_or_create_account(issue.fields.assignee.key,
                                                       full_name=issue.fields.assignee.displayName)
        bug.product = None
        bug.component = ",".join([c.name for c in issue.fields.components])

        # Get the list of comments
        if bug.id is not None and bug.has_new_comments:
            now = timezone.now()
            for c in issue.fields.comment.comments:
                try:
                    account = self.find_or_create_account(c.author.name,
                                                          full_name=c.author.displayName)
                    url = "{}#comment-{}".format(bug.url, c.id)
                    BugComment.objects.create(bug=bug, account=account,
                                              comment_id=c.id, url=url,
                                              created_on=c.created)
                except IntegrityError:
                    # We may have already imported the comment
                    pass
            bug.comments_polled = now

        # TODO: Handle features_field and platforms_field

    def __list_to_jql(self, objects):
        return ", ".join(['"{}"'.format(o) for o in objects])

    def search_bugs_ids(self, components=None, created_since=None, status=None):
        query = ["issuetype = Bug"]

        if components is not None:
            query.append("component in ({})".format(self.__list_to_jql(components)))

        if created_since is not None:
            query.append("created > \"{}\"".format(created_since.strftime("%Y/%m/%d %H:%M")))

        if status is not None:
            query.append("status in ({})".format(self.__list_to_jql(status)))

        jql_str = " AND ".join(query)
        return set([i.key for i in self.jira.search_issues(jql_str)])

    def add_comment(self, bug, comment):
        issue = self.jira.issue(bug.bug_id)
        self.jira.add_comment(issue, str(comment))

    def create_bug(self, bug):
        self._can_create(bug)

        title = bug.title
        description = bug.description
        project_key = bug.tracker.project

        issue_fields = dict()
        issue_fields['fields'] = dict()
        fields = issue_fields['fields']
        fields['project'] = {'key': project_key}
        fields['issuetype'] = {'name': "Bug"}
        fields['summary'] = title
        fields['description'] = description

        new_issue = self.jira.create_issue(fields=issue_fields)

        # new bug ID will be used to create the bug in the database
        return new_issue.id


class GitLab(BugTrackerCommon):
    GET = "get"
    POST = "post"

    def __init__(self, db_bugtracker):
        super().__init__(db_bugtracker)

    def __make_json_request(self, url, params={}, method="get"):
        headers = {'PRIVATE-TOKEN': self.db_bugtracker.password}

        request_method = getattr(requests, method)
        response = request_method(url, params=params, headers=headers)

        response.raise_for_status()

        return response.json()

    def __json_user(self, json):
        return self.find_or_create_account(json['id'], full_name=json['name'])

    def __get_issues(self, query):
        url = self.url
        return self.__make_json_request(url, params=query)

    def __get_issue(self, issue_iid):
        url = self.join(self.url, str(issue_iid))
        return self.__make_json_request(url)

    def __get_notes_url(self, issue_iid):
        note_url = str(issue_iid) + "/notes"
        return self.join(self.url, note_url)

    def __poll_comments(self, bug, web_url):
        now = timezone.now()
        notes = self.__make_json_request(self.__get_notes_url(bug.bug_id))
        for note in notes:
            note_id = note['id']
            author = self.__json_user(note['author'])
            url = "{}#note_{}".format(web_url, note_id)
            created_on = note['created_at']

            if not BugComment.objects.filter(bug=bug, comment_id=note_id).exists():
                BugComment.objects.create(bug=bug,
                                          account=author,
                                          comment_id=note_id,
                                          url=url,
                                          created_on=created_on)
        bug.comments_polled = now

    @property
    def open_statuses(self):
        return ['opened']

    @property
    def url(self):
        project_id = self.db_bugtracker.project
        base_url = self.db_bugtracker.url
        proj_url = "api/v4/projects/{}/issues".format(project_id)
        return self.join(base_url, proj_url) + "/"

    def poll(self, bug):
        issue = self.__get_issue(bug.bug_id)

        bug.title = issue['title']
        bug.status = issue['state']
        bug.description = issue['description']

        bug.created = issue['created_at']
        bug.updated = issue['updated_at']
        bug.closed = issue['closed_at']  # None if not closed

        bug.creator = self.__json_user(issue['author'])

        if issue['assignee'] is not None:
            bug.assignee = self.__json_user(issue['assignee'])

        bug.product = None
        bug.component = None

        if bug.id is not None and bug.has_new_comments:
            self.__poll_comments(bug, issue['web_url'])

    def search_bugs_ids(self, components=None, created_since=None, status=None):
        query = {}

        # no component field in GitLab

        if created_since is not None:
            query['created_after'] = created_since

        if status is not None:
            query['state'] = status

        issues = self.__get_issues(query)
        iids = map(lambda x: str(x['iid']), issues)

        return set(iids)

    def add_comment(self, bug, comment):
        url = self.__get_notes_url(bug.bug_id)
        self.__make_json_request(url, params={'body': str(comment)}, method=GitLab.POST)

    def create_bug(self, bug):
        self._can_create(bug)

        title = bug.title
        description = bug.description

        params = {'title': title,
                  'description': description,
                  'labels': "Bug"}
        new_issue = self.__make_json_request(self.url, params=params, method='post')

        # new bug ID will be used to create the bug in the database
        return new_issue['iid']
