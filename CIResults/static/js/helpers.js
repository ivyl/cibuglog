/*
 * Adds a row to the tbody of a table identified by an id (#mytable) that has
 * an attribute data-row-next-id set,.
 *
 * WARNING: if data-row-next-id is set to 0, then the tbody is cleared before
 *          adding anything. This is useful to add placeholders.
 */
function table_add_row(table_id) {
    // get an id for the new row, and increment the storage
    var id = $(table_id).data("row-next-id")
    $(table_id).data("row-next-id", id + 1)

    // Remove the current content (placeholder) if the the id is 0
    if (id == 0)
        $(table_id+" tbody").html("");

    // Create a new row
    row = $("<tr></tr>")
    row.data('id', id)

    // Move the row to the right location
    $(table_id+" tbody").append(row)

    return row
}

function handleRemoveRow() {
    $('[data-remove-row]').click(function() {
        $(this).closest("tr").remove();
    });
}

function handleEditFilter() {
    $("[data-edit-filter]").click(function(click){
        var select=$($($(click.target).parents()[1]).find("select"))
        $(window).trigger('issue-edit-filter', select.val());
    });
}

function makeDualListBox(name) {
    var filter_tags_select = $('select[name="'+name+'"]').bootstrapDualListbox({
        nonSelectedListLabel: 'Ignored',
        selectedListLabel: 'Selected',
        moveOnSelect: false,
        selectorMinimalHeight: 175,
        bootstrap3Compatible: true,
    });
}

function dualListBoxSelected(name) {
    var ret = [];
    var objs = $('[name="'+name+'"]').val();
    for (var o in objs)
        ret.push(objs[o]);
    return ret;
}

function arrayToInt(array) {
    for (var i in array)
        array[i] = parseInt(array[i], 10);
    return array;
}

function dualListBoxReset(name) {
    $('[name="'+name+'"]').val([])
    $('[name="'+name+'"]').bootstrapDualListbox('refresh')
}

function do_background_post_request(url, attributes) {
    var formData = new FormData();
    formData.append("csrfmiddlewaretoken", $("[name=csrfmiddlewaretoken]").val());
    for(var key in attributes) {
        formData.append(key, attributes[key]);
    }

    var request = new XMLHttpRequest();
    request.open("POST", url);
    request.send(formData);
}

function handleConfirmations() {
    var current = null;

    /* Find all the href with data-confirmation */
    $('a[data-confirmation]').click(function() {
        current = this
        eModal.confirm($(this).data("confirmation"), null).then(function() {
            do_background_post_request(current.href, {});
            if ($(current).data('confirmation-reload') !== undefined) {
                location.reload();
            }
            current = null;
        });
        return false;
    });

    /* For forms */
    $('button[data-submit-confirm]').click(function() {
        current_form = $(this).closest("form")
        checked = $(current_form).find('input[type="checkbox"]:checked').length

        msg = $(this).data("submit-confirm")
        msg = msg.replace("{checked}", checked)

        eModal.confirm(msg, null).then(function() {
            $(current_form).submit()
        });
        return false;
    });
}

function handleInforms() {
    /* Find all the href with data-confirmation */
    $('a[data-inform]').click(function() {
        eModal.alert($(this).data("inform"), "Information");
        return false;
    });

    /* Find all the href with data-confirmation */
    $('a[data-inform-target]').click(function() {
        eModal.alert($("#" + $(this).data("inform-target")).html(), "Information");
        return false;
    });
}

function handleSelectAll() {
    /* Add a checkbox */
    $('[data-select-name]').each(function(){
        $(this).prepend('<span class="select-item"><input type="checkbox" name="' + $(this).data("select-name") + '"/></span>');
    });

    $('a[data-select-all]').click(function() {
        $("#" + $(this).data("select-all")).find("input").prop('checked', true);
        return false;
    });

    $('a[data-unselect-all]').click(function() {
        $("#" + $(this).data("unselect-all")).find("input").prop('checked', false);
        return false;
    });
}

function handleDualListBox() {
    $('[data-dual-list-box]').each(function() {
        makeDualListBox($(this).attr('name'));
    })
}

function setAccountIsDev(account_id, is_dev) {
    var xhr = new XMLHttpRequest();

    // FIXME: Find a way not to hardcode the URL here...
    xhr.open('PATCH', "../api/bugtrackeraccount/" + account_id + "/");
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {
        if (xhr.status === 200) {
            location.reload();
        }
    };
    xhr.send(JSON.stringify({"is_developer": is_dev}));
}

function handleAccountEdit() {
    /* Find all the href with data-confirmation */
    $('a[data-account-edit]').click(function() {
        var account_id = $(this).data("account-edit")

        var options = {
            message: 'What role should this account have?',
            title: "Account role selection",
            size: eModal.size.sm,
            buttons: [
                {text: 'Cancel', style: 'default',   close: true, click: function() {
                    // Nothing to do
                }
                },
                {text: 'User', style: 'success',   close: true, click: function() {
                        setAccountIsDev(account_id, false);
                    }
                },
                {text: 'Dev', style: 'primary',   close: true, click: function() {
                        setAccountIsDev(account_id, true);
                    }
                },
            ],
        };

        eModal.alert(options);
        return false;
    });
}

function formDisableOnSubmit() {
    $('form[data-disable-on-submit').submit(function() {
        txt = $(this).data('disable-on-submit')
        $("[type='submit']", this)
            .html(txt)
            .attr('disabled', 'disabled');

        return true;
    });
}


/* Handle all the start-up scripts */
$(document).ready(function() {
    handleRemoveRow();

    handleConfirmations();

    handleInforms();

    handleSelectAll();

    handleDualListBox();

    handleAccountEdit();

    formDisableOnSubmit();
})
