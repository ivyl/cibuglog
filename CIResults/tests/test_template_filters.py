from django.test import TestCase
from django.utils import timezone

from CIResults.templatetags.runconfig_diff import show_test

from CIResults.models import Test, RunConfig


class ShowTestTests(TestCase):
    def test_new_test(self):
        test = Test(name="test", first_runconfig=None, vetted_on=None)
        self.assertEqual(show_test(test), "{test} (NEW)")

    def test_suppressed_test(self):
        test = Test(name="test", first_runconfig=RunConfig(), vetted_on=None)
        self.assertEqual(show_test(test), "{test}")

    def test_active_test(self):
        test = Test(name="test", first_runconfig=RunConfig(), vetted_on=timezone.now())
        self.assertEqual(show_test(test), "test")
