from django.test import TestCase

from CIResults.metrics import Periodizer

import datetime


class PeriodizerTests(TestCase):
    def __common_checks(self, periods):
        self.assertEqual(periods[0].start, datetime.datetime(2018, 3, 19, 0, 0))
        self.assertEqual(periods[0].end, datetime.datetime(2018, 3, 26, 0, 0))
        self.assertEqual(periods[-1].start, datetime.datetime(2018, 10, 8, 0, 0))
        self.assertEqual(periods[-1].end, datetime.datetime(2018, 10, 15, 0, 0))

        self.assertEqual(len(periods), 30)

        # Check that the time between each day is the expected one
        for p, period in enumerate(periods):
            self.assertEqual(period.end - period.start, datetime.timedelta(days=7))
            if p > 1:
                self.assertEqual(period.start, periods[p-1].end)

    def test_monday_midnight(self):
        periods = list(Periodizer(end_date=datetime.datetime(year=2018, month=10, day=8,
                                                             hour=0, minute=0, second=0)))
        self.__common_checks(periods)

    def test_wednesday(self):
        periods = list(Periodizer(end_date=datetime.datetime(year=2018, month=10, day=10,
                                                             hour=14, minute=23, second=10)))
        self.__common_checks(periods)

    def test_sunday_1s_to_midnight(self):
        periods = list(Periodizer(end_date=datetime.datetime(year=2018, month=10, day=14,
                                                             hour=23, minute=59, second=59)))
        self.__common_checks(periods)
