from unittest.mock import patch
from rest_framework.test import APIRequestFactory
from django.conf import settings
from django.test import TestCase
from django.urls import reverse

from CIResults.models import Test, Machine, RunConfigTag, TestSuite
from CIResults.models import TextStatus, IssueFilter, Issue, MachineTag
from CIResults.views import REST_IssueFilterViewSet


# HACK: Massively speed up the login primitive. We don't care about security in tests
settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )


class ViewMixin:
    @property
    def private_url(self):
        if not hasattr(self, 'private_view'):
            self.skipTest("No private view set for the class")  # pragma: no cover
        return reverse(self.private_view)

    @property
    def public_url(self):
        if not hasattr(self, 'public_view'):
            self.skipTest("No public view set for the class")  # pragma: no cover
        return reverse(self.public_view)

    def test_get_private(self):
        response = self.client.get(self.private_url)
        self.assertEqual(response.status_code, 200)

    def test_get_public(self):
        # Url checks
        self.assertTrue(self.public_url.endswith('.html'))

        response = self.client.get(self.public_url)
        self.assertEqual(response.status_code, 200)

        # Check that we have no history links
        self.assertNotContains(response, "/history")


class UserFiltrableViewMixin(ViewMixin):
    def test_invalid_query(self):
        response = self.client.get(self.private_url + "?query=djzkhjkf")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Filtering error")

    def test_valid_query(self):
        response = self.client.get(self.private_url + "?query=" + self.query)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "Filtering error")


class IndexTests(ViewMixin, TestCase):
    private_view = "CIResults-index"
    public_view = "CIResults-index-public"


class IssueListTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-issues-list"
    public_view = "CIResults-issues-list-public"
    query = "filter_description = 'desc'"


class TestTests(ViewMixin, TestCase):
    private_view = "CIResults-tests"
    public_view = "CIResults-tests-public"


class TestMassRenameTests(ViewMixin, TestCase):
    private_view = "CIResults-tests-massrename"


class MachineTests(ViewMixin, TestCase):
    private_view = "CIResults-machines"
    public_view = "CIResults-machines-public"


class TestResultListViewTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-results"
    query = "test_name = 'test'"


class KnownFailureListViewTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-knownfailures"
    query = "test_name = 'test'"


class MetricsOverviewTests(ViewMixin, TestCase):
    private_view = "CIResults-metrics-overview"
    public_view = "CIResults-metrics-overview-public"


class MetricsBugsTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-bugs"
    public_view = "CIResults-metrics-bugs-public"
    query = "status = 'open'"


class MetricsOpenBugsTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-open-bugs"
    public_view = "CIResults-metrics-open-bugs-public"
    query = "product = 'test'"


class MetricsPassrateTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-passrate"
    public_view = "CIResults-metrics-passrate-public"
    query = "test_name = 'test'"


class MetricsRunTimeTests(UserFiltrableViewMixin, TestCase):
    private_view = "CIResults-metrics-runtime"
    public_view = "CIResults-metrics-runtime-public"
    query = "machine_name = 'machine'"


class REST_IssueFilterTests(TestCase):
    def setUp(self):
        self.view = REST_IssueFilterViewSet()
        self.arf = APIRequestFactory()

    def __post__(self, body_dict, addr='/'):
        request = self.arf.post(addr, body_dict, format='json')
        return self.view.create(request)

    def test_create_empty(self):
        response = self.__post__({})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, ["The field 'description' cannot be empty"])

    def test_invalid_regexps(self):
        response = self.__post__({"description": "Minimal IssueFilter",
                                  "stdout_regex": '(', "stderr_regex": '(',
                                  "dmesg_regex": '('})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, ["The field 'stdout_regex' does not contain a valid regular expression",
                                         "The field 'stderr_regex' does not contain a valid regular expression",
                                         "The field 'dmesg_regex' does not contain a valid regular expression"])

    def test_create_minimal(self):
        response = self.__post__({"description": "Minimal IssueFilter"})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data,
                         {'id': 1, 'description': 'Minimal IssueFilter',
                          'tags': [], 'machine_tags': [], 'machines': [], 'tests': [],
                          'statuses': [], 'stdout_regex': '',
                          'stderr_regex': '', 'dmesg_regex': '',
                          '__str__': 'Minimal IssueFilter'})

    def test_create_invalid(self):
        response = self.__post__({
            "description": "Invalid tags, machines, tests, and statuses",
            'tags': [1], 'machines': [2], 'tests': [3], 'statuses': [4]
            })
        self.assertEqual(response.status_code, 400, response.data)
        self.assertEqual(response.data, ['At least one tag does not exist',
                                         'At least one machine does not exist',
                                         'At least one test does not exist',
                                         'At least one status does not exist'])

    def test_create_complete(self):
        # Create some objects before referencing them
        ts = TestSuite.objects.create(name="ts", description="", url="", public=True)
        for i in range(1, 6):
            RunConfigTag.objects.create(id=i, name="tag{}".format(i), description="", url="", public=True)
            Machine.objects.create(id=i, name="machine{}".format(i), public=True)
            Test.objects.create(id=i, name="test{}".format(i), testsuite=ts, public=True)
            TextStatus.objects.create(testsuite=ts, name="status{}".format(i))
            MachineTag.objects.create(name="TAG{}".format(i), public=True)

        # Make the request and check that we get the expected output
        response = self.__post__({
            "description": "Minimal IssueFilter",
            'tags': [1, 2], 'machine_tags': [1, 5], 'machines': [2, 3], 'tests': [3, 4], 'statuses': [4, 5],
            'stdout_regex': 'stdout', 'stderr_regex': 'stderr', 'dmesg_regex': 'dmesg'
            })
        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(response.data,
                         {'id': 1, 'description': 'Minimal IssueFilter',
                          'tags': [
                              {'id': 1, 'description': '', 'url': '', 'public': True, '__str__': 'tag1'},
                              {'id': 2, 'description': '', 'url': '', 'public': True, '__str__': 'tag2'}
                              ],
                          'machine_tags': [
                              {'id': 1, 'name': 'TAG1', 'public': True},
                              {'id': 5, 'name': 'TAG5', 'public': True},
                              ],
                          'machines': [
                              {'id': 2, 'vetted_on': None, 'public': True, '__str__': 'machine2'},
                              {'id': 3, 'vetted_on': None, 'public': True, '__str__': 'machine3'},
                              ],
                          'tests': [
                              {'id': 3, 'testsuite': {'id': 1, '__str__': 'ts'}, 'public': True, 'vetted_on': None,
                               '__str__': 'ts: test3'},
                              {'id': 4, 'testsuite': {'id': 1, '__str__': 'ts'}, 'public': True, 'vetted_on': None,
                               '__str__': 'ts: test4'},
                              ],
                          'statuses': [
                              {'id': 4, 'testsuite': {'id': 1, '__str__': 'ts'}, '__str__': 'ts: status4'},
                              {'id': 5, 'testsuite': {'id': 1, '__str__': 'ts'}, '__str__': 'ts: status5'},
                              ],
                          'stdout_regex': 'stdout', 'stderr_regex': 'stderr',
                          'dmesg_regex': 'dmesg', '__str__': 'Minimal IssueFilter'})

    def test_edit_invalid(self):
        response = self.__post__({"description": "new filter",
                                  "edit_filter": "a", "edit_issue": "b"})
        self.assertEqual(response.status_code, 400, response.data)
        self.assertEqual(response.data, ["The field 'edit_filter' needs to be an integer",
                                         "The field 'edit_issue' needs to be an integer"])

    @patch('CIResults.models.IssueFilter.replace')
    @patch('CIResults.models.Issue.replace_filter')
    def test_edit_all_issues(self, mock_replace_filter, mock_filter_replace):
        filter = IssueFilter.objects.create(description="old filter")

        response = self.__post__({"description": "new filter", "edit_filter": filter.id})
        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(response.data,
                         {'id': 2, 'description': 'new filter',
                          'tags': [], 'machine_tags': [], 'machines': [], 'tests': [],
                          'statuses': [], 'stdout_regex': '',
                          'stderr_regex': '', 'dmesg_regex': '',
                          '__str__': 'new filter'})

        new_filter = IssueFilter.objects.get(pk=2)
        mock_replace_filter.assert_not_called()
        mock_filter_replace.assert_called_once_with(new_filter)
        self.assertEqual(mock_filter_replace.call_args_list[0][0][0].id, 2)

    @patch('CIResults.models.IssueFilter.replace')
    @patch('CIResults.models.Issue.replace_filter')
    def test_edit_one_issue(self, mock_replace_filter, mock_filter_replace):
        filter = IssueFilter.objects.create(description="desc")
        issue = Issue.objects.create(description="issue")

        response = self.__post__({"description": "new filter",
                                  "edit_filter": "{}".format(filter.id),
                                  "edit_issue": issue.id})
        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(response.data,
                         {'id': 2, 'description': 'new filter',
                          'tags': [], 'machine_tags': [], 'machines': [], 'tests': [],
                          'statuses': [], 'stdout_regex': '',
                          'stderr_regex': '', 'dmesg_regex': '',
                          '__str__': 'new filter'})

        new_filter = IssueFilter.objects.get(pk=2)
        mock_filter_replace.assert_not_called()
        mock_replace_filter.assert_called_once_with(filter, new_filter)
        self.assertEqual(mock_replace_filter.call_args_list[0][0][0].id, filter.id)
        self.assertEqual(mock_replace_filter.call_args_list[0][0][1].id, 2)
