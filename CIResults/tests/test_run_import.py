from unittest.mock import patch, Mock, call, mock_open
from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
from io import StringIO

from CIResults.models import Component, Build, Test, Machine, RunConfig, TestSuite, TestsuiteRun
from CIResults.models import TextStatus, TestResult, KnownFailure, RunFilterStatistic, UnknownFailure
from CIResults.run_import import BuildResult, TestsuiteRunResults, TestsuiteTestResult, TestsuiteResults
from CIResults.run_import import RunConfigResults, PiglitResult, TestSuiteRunDef

import configparser
import datetime


class BuildResultTests(TestCase):

    def __mock_configparser__(self):
        # Replace the configparser instanciated by RunConfigResult with our own
        self.config = configparser.ConfigParser()
        patcher_api_call = patch('configparser.ConfigParser')
        mock_matches = patcher_api_call.start()
        mock_matches.return_value = self.config
        self.addCleanup(patcher_api_call.stop)

        # Make the read() function a noop
        self.config.read = lambda x: 0

    def test_parse_run_info__empty(self):
        self.__mock_configparser__()

        # Check empty
        self.config.read_string("")
        self.assertRaisesMessage(ValueError,
                                 "The build info file build.ini is invalid: missing the section CIRESULTS_BUILD",
                                 BuildResult, "")

    def test_parse_run_info__field_missing(self):
        self.__mock_configparser__()

        cfg = """[CIRESULTS_BUILD]"""
        for field in ['name', 'component', 'version']:
            self.config.read_string(cfg)
            msg = "The build info file build.ini is invalid: the required field '{}' is missing".format(field)
            self.assertRaisesMessage(ValueError, msg, BuildResult, "")

            cfg += "\n    {} = value".format(field)

    def test_parse_run_info__more_sections(self):
        self.__mock_configparser__()

        cfg = """[CIRESULTS_BUILD]
        field = value

        [SECTION]
        field = value"""
        self.config.read_string(cfg)
        msg = "The build info file build.ini is invalid: only the section CIRESULTS_BUILD is allowed"
        self.assertRaisesMessage(ValueError, msg, BuildResult, "")

    def test_parse_run_info__minimal(self):
        self.__mock_configparser__()

        component = Component.objects.create(name="COMPONENT1", description="desc",
                                             url="url", public=True)

        cfg = """[CIRESULTS_BUILD]
        name: BUILD2
        component: COMPONENT1
        version: VERSION1"""
        self.config.read_string(cfg)
        build = BuildResult("")
        build.commit_to_db()

        obj = Build.objects.get(name="BUILD2")
        self.assertEqual(obj.name, "BUILD2")
        self.assertEqual(obj.component, component)
        self.assertEqual(obj.version, "VERSION1")
        self.assertEqual(set(obj.parents.all()), set())
        self.assertEqual(obj.branch, None)
        self.assertEqual(obj.repo_type, None)
        self.assertEqual(obj.repo, None)
        self.assertEqual(obj.upstream_url, None)  # Check that the invalid URL was not used
        self.assertEqual(obj.parameters, None)
        self.assertEqual(obj.build_log, None)

    @patch('builtins.open', mock_open(read_data="Parameters\nfile"))
    def test_parse_run_info__parameters_file(self):
        self.__mock_configparser__()

        Component.objects.create(name="COMPONENT1", description="desc", url="url", public=True)

        cfg = """[CIRESULTS_BUILD]
        name: BUILD1
        component: COMPONENT1
        version: VERSION1
        parameters_file: file"""
        self.config.read_string(cfg)
        build = BuildResult("")
        build.commit_to_db()

        obj = Build.objects.get(name="BUILD1")
        self.assertEqual(obj.parameters, "Parameters\nfile")

    @patch('builtins.open', mock_open(read_data="Parameters\nfile"))
    def test_parse_run_info__parameters_priority(self):
        self.__mock_configparser__()

        Component.objects.create(name="COMPONENT1", description="desc", url="url", public=True)

        cfg = """[CIRESULTS_BUILD]
        name: BUILD1
        component: COMPONENT1
        version: VERSION1
        parameters: parameters inline
        parameters_file: file"""
        self.config.read_string(cfg)
        build = BuildResult("")
        build.commit_to_db()

        obj = Build.objects.get(name="BUILD1")
        self.assertEqual(obj.parameters, "parameters inline")

    @patch('builtins.open', mock_open(read_data="My\nBuild\nLog"))
    def test_parse_run_info__complete(self):
        self.__mock_configparser__()

        # Create some objects in the DB that are dependencies for the new build
        component = Component.objects.create(name="COMPONENT1", description="desc",
                                             url="url", public=True)
        builds = []
        for i in range(0, 2):
            b = Build.objects.create(name="BUILD{}".format(i), component=component,
                                     version="version")
            builds.append(b)

        # Check a complete configuration
        cfg = """[CIRESULTS_BUILD]
        name: BUILD2
        component: COMPONENT1
        version: VERSION1
        parents: BUILD0  BUILD1
        branch: BRANCH1
        repo_type: REPO_TYPE1
        repo: REPO1
        upstream_url: http://URL1.de
        parameters: PARAMETERS WITH SPACES
            AND
            NEW
            LINES
        build_log_file: logfile
        """
        self.config.read_string(cfg)
        build = BuildResult("")
        build.commit_to_db()

        obj = Build.objects.get(name="BUILD2")
        self.assertEqual(obj.name, "BUILD2")
        self.assertEqual(obj.component, component)
        self.assertEqual(obj.version, "VERSION1")
        self.assertEqual(set(obj.parents.all()), set(builds))
        self.assertEqual(obj.branch, "BRANCH1")
        self.assertEqual(obj.repo_type, "REPO_TYPE1")
        self.assertEqual(obj.repo, "REPO1")
        self.assertEqual(obj.upstream_url, "http://URL1.de")
        self.assertEqual(obj.parameters, "PARAMETERS WITH SPACES\nAND\nNEW\nLINES")
        self.assertEqual(obj.build_log, "My\nBuild\nLog")

    @patch('builtins.open', mock_open(read_data="My\nBuild\nLog"))
    def test_parse_run_info__no_ini__complete(self):
        # Create some objects in the DB that are dependencies for the new build
        component = Component.objects.create(name="COMPONENT1", description="desc",
                                             url="url", public=True)
        builds = []
        for i in range(0, 2):
            b = Build.objects.create(name="BUILD{}".format(i), component=component,
                                     version="version")
            builds.append(b)

        # Check a complete configuration
        build = BuildResult(name="BUILD2", component="COMPONENT1", version="VERSION1",
                            parents=["BUILD0", "BUILD1"], branch="BRANCH1",
                            repo_type="REPO_TYPE1", repo="REPO1",
                            upstream_url="http://URL1.de",
                            parameters="PARAMETERS WITH SPACES\nAND\nNEW\nLINES",
                            config_file="", build_log_file="logfile")
        build.commit_to_db()

        obj = Build.objects.get(name="BUILD2")
        self.assertEqual(obj.name, "BUILD2")
        self.assertEqual(obj.component, component)
        self.assertEqual(obj.version, "VERSION1")
        self.assertEqual(set(obj.parents.all()), set(builds))
        self.assertEqual(obj.branch, "BRANCH1")
        self.assertEqual(obj.repo_type, "REPO_TYPE1")
        self.assertEqual(obj.repo, "REPO1")
        self.assertEqual(obj.upstream_url, "http://URL1.de")
        self.assertEqual(obj.parameters, "PARAMETERS WITH SPACES\nAND\nNEW\nLINES")
        self.assertEqual(obj.build_log, "My\nBuild\nLog")

    def test_parse_run_info__no_ini__parents_is_None(self):
        # Create some objects in the DB that are dependencies for the new build
        component = Component.objects.create(name="COMPONENT1", description="desc",
                                             url="url", public=True)
        builds = []
        for i in range(0, 2):
            b = Build.objects.create(name="BUILD{}".format(i), component=component,
                                     version="version")
            builds.append(b)

        # Check a complete configuration
        build = BuildResult(name="BUILD2", component="COMPONENT1", version="VERSION1",
                            parents=None)
        build.commit_to_db()

        obj = Build.objects.get(name="BUILD2")
        self.assertEqual(obj.name, "BUILD2")
        self.assertEqual(obj.component, component)
        self.assertEqual(obj.version, "VERSION1")
        self.assertEqual(set(obj.parents.all()), set())


class TestsuiteRunResultsTests(TestCase):
    def test___result_url__(self):
        pattern = "http://hello.world/{runconfig}/{machine}/{testsuite_build}/{run_id}/{test}"
        testsuite = Mock(build="build1", result_url_pattern=pattern, runconfig=Mock())
        testsuite.runconfig.name = "runcfg"

        self.assertEqual(TestsuiteRunResults.__result_url__(testsuite, 42, "machine1", "test1"),
                         "http://hello.world/runcfg/machine1/build1/42/test1")


class TestsuiteResultsTests(TestCase):
    def setUp(self):
        self.runconfig = Mock(name="runcfg")

    @patch('CIResults.models.Build.objects.get', return_value=Build(component=Component()))
    @patch('CIResults.models.TestSuite.objects.get', return_value=TestSuite())
    def test_sanity(self, ts_mock, build_mock):
        ts_run = TestsuiteResults(self.runconfig, "name1", "build1", "piglit", 1, '')
        self.assertEqual(self.runconfig, self.runconfig)
        self.assertEqual(ts_run.name, "name1")
        self.assertEqual(ts_run.build, "build1")
        self.assertTrue(build_mock.called_once())
        self.assertTrue(ts_mock.called_once())
        self.assertEqual(type(ts_run.db_object), TestSuite)
        self.assertEqual(ts_run.format, "piglit")
        self.assertEqual(ts_run.version, 1)
        self.assertEqual(ts_run._result_type, PiglitResult)

    @patch('CIResults.models.Build.objects.get', return_value=Build(component=Component()))
    @patch('CIResults.models.TestSuite.objects.get', return_value=TestSuite())
    def test_invalid_format(self, ts_mock, build_mock):
        self.assertRaisesMessage(ValueError, "The testsuite result format 'nonexisting' is unsupported",
                                 TestsuiteResults, self.runconfig, "name1", "build1", "nonexisting", 1, '')

        self.assertRaisesMessage(ValueError, "The version 0 of the testsuite result format 'piglit' is unsupported",
                                 TestsuiteResults, self.runconfig, "name1", "build1", "piglit", 0, '')

    @patch('CIResults.models.Build.objects.get', return_value=Build(component=Component()))
    @patch('CIResults.models.TestSuite.objects.get', return_value=TestSuite())
    @patch('CIResults.run_import.PiglitResult', return_value=None)
    def test_read_results(self, piglitresult_mock, ts_mock, build_mock):
        ts_run = TestsuiteResults(self.runconfig, "name1", "build1", "piglit", 1, '')
        ts_run.read_results("machine1", "1", "path/to/results")
        self.assertTrue(piglitresult_mock.called_once())


class TestSuiteRunDefTests(TestCase):
    def test_init(self):
        for field in ["testsuite_build", "results_format", "results_format_version",
                      "machine", "ts_run_id", "ts_run_path"]:
            kargs = {"testsuite_build": "BUILD", "results_format": "piglit",
                     "results_format_version": 1, "machine": "MACHINE",
                     "ts_run_id": 0, "ts_run_path": ""}
            kargs[field] = None

            msg = "The parameter {} cannot be None".format(field)
            if field == "results_format_version" or field == "ts_run_id":
                msg = "The parameter {} 'None' should be an integer".format(field)

            self.assertRaisesMessage(ValueError, msg, TestSuiteRunDef, **kargs)


class RunConfigResultsTests(TestCase):

    def __mock_configparser__(self):
        # Replace the configparser instanciated by RunConfigResult with our own
        self.config = configparser.ConfigParser()
        patcher_api_call = patch('configparser.ConfigParser')
        mock_matches = patcher_api_call.start()
        mock_matches.return_value = self.config
        self.addCleanup(patcher_api_call.stop)
        patcher_api_call = patch('configparser.ConfigParser.read')
        mock_matches = patcher_api_call.start()
        mock_matches.return_value = None
        self.addCleanup(patcher_api_call.stop)

    def test_parse_run_info__empty(self):
        self.__mock_configparser__()

        # Check empty
        self.config.read_string("")
        self.assertRaisesMessage(ValueError, "The RunConfig file runconfig.ini is invalid",
                                 RunConfigResults, "")

    def test_parse_run_info__name_missing(self):
        self.__mock_configparser__()

        # Check name missing
        cfg = """[CIRESULTS_RUNCONFIG]
        """
        self.config.read_string(cfg)
        self.assertRaisesMessage(ValueError, "The RunConfig file runconfig.ini is invalid: runconfig name unspecified",
                                 RunConfigResults, "")

    @patch('os.listdir', return_value=[])
    def test_parse_run_info__minimal(self, mocked_listdir):
        self.__mock_configparser__()

        cfg = """[CIRESULTS_RUNCONFIG]
        name: RUNCFG1
        """
        self.config.read_string(cfg)
        runcfg = RunConfigResults("")
        self.assertEqual(runcfg.name, "RUNCFG1")
        self.assertEqual(runcfg.url, None)
        self.assertEqual(runcfg.environment, None)
        self.assertEqual(runcfg.builds, [])
        self.assertEqual(runcfg.tags, [])
        self.assertEqual(runcfg.testsuites, {})
        self.assertFalse(runcfg.temporary)

    def test_parse_run_info__invalid_build(self):
        self.__mock_configparser__()

        cfg = """[CIRESULTS_RUNCONFIG]
        name: RUNCFG1

        [Testsuite1]
        build: build1
        """
        self.config.read_string(cfg)
        msg = "The build 'build1' of the testsuite 'Testsuite1' is not found in the list of builds "
        msg += "of the runconfig RUNCFG1"
        self.assertRaisesMessage(ValueError, msg, RunConfigResults, "")

    def test_parse_run_info__invalid_single_testsuite(self):
        self.__mock_configparser__()

        cfg = """[CIRESULTS_RUNCONFIG]
        name: RUNCFG1
        builds: build1 build2

        [CIRESULTS_TESTSUITE]
        build: build1

        [Testsuite2]
        build: build2
        """
        self.config.read_string(cfg)
        msg = "If the section CIRESULTS_TESTSUITE exists, then no additional section/testsuite can be added"
        self.assertRaisesMessage(ValueError, msg, RunConfigResults, "")

    @patch('os.listdir', return_value=[])
    @patch('CIResults.run_import.TestsuiteResults')
    def test_parse_run_info__multi_complete(self, mocked_ts_run, mocked_listdir):
        self.__mock_configparser__()

        # Check a complete configuration
        cfg = """[CIRESULTS_RUNCONFIG]
        name: RUNCFG2
        url: http://test.url.com
        environment: blablabla
            blibliblib
        builds: build1 build2 build3
        tags: tag1 tag2 tag3
        temporary: true

        [Testsuite1]
        build: build2
        format: format1
        result_url_pattern = pattern1

        [Testsuite2]
        build: build3
        format: format2
        version: 2
        result_url_pattern = pattern2
        """
        self.config.read_string(cfg)
        runcfg = RunConfigResults("")
        self.assertEqual(runcfg.name, "RUNCFG2")
        self.assertEqual(runcfg.url, "http://test.url.com")
        self.assertEqual(runcfg.environment, "blablabla\nblibliblib")
        self.assertEqual(runcfg.builds, ['build1', 'build2', 'build3'])
        self.assertEqual(runcfg.tags, ['tag1', 'tag2', 'tag3'])
        self.assertEqual(set(runcfg.testsuites.keys()),
                         set(["Testsuite1", "Testsuite2"]))
        self.assertTrue(runcfg.temporary)

        self.assertEqual(len(mocked_ts_run.call_args_list), 2)
        mocked_ts_run.assert_any_call(runcfg, 'Testsuite1', 'build2', 'format1', 1, 'pattern1')
        mocked_ts_run.assert_any_call(runcfg, 'Testsuite2', 'build3', 'format2', 2, 'pattern2')

    @patch('os.listdir', return_value=[])
    @patch('CIResults.run_import.TestsuiteResults')
    @patch('CIResults.run_import.RunConfigResults.__load_testsuite_results__')
    def test_parse_run_info__single_complete(self, mocked_load_results, mocked_ts_run, mocked_listdir):
        self.__mock_configparser__()

        cfg = """[CIRESULTS_RUNCONFIG]
        name: RUNCFG2
        builds: build1 build2 build3
        tags: tag1 tag2 tag3

        [CIRESULTS_TESTSUITE]
        build: build2
        format: format1
        """
        self.config.read_string(cfg)
        runcfg = RunConfigResults("")
        self.assertEqual(runcfg.name, "RUNCFG2")
        self.assertEqual(runcfg.builds, ['build1', 'build2', 'build3'])
        self.assertEqual(runcfg.tags, ['tag1', 'tag2', 'tag3'])
        self.assertEqual(set(runcfg.testsuites.keys()),
                         set(["CIRESULTS_TESTSUITE"]))

        self.assertEqual(len(mocked_ts_run.call_args_list), 1)
        mocked_ts_run.assert_any_call(runcfg, 'CIRESULTS_TESTSUITE', 'build2', 'format1', 1, "")

        self.assertEqual(len(mocked_load_results.call_args_list), 1)
        mocked_load_results.assert_any_call(runcfg._testsuites.get("CIRESULTS_TESTSUITE"), "")

    @patch('sys.stdout', new_callable=StringIO)
    @patch('CIResults.run_import.TestsuiteResults')
    def test_load_results(self, mocked_ts_run, mocked_stdout):
        runcfg = RunConfigResults("CIResults/tests/results")

        self.assertEqual(set(mocked_stdout.getvalue().splitlines()),
                         set(["RunConfigResults: testsuite run ID 'invalid_id' should be an integer",
                              "Ignore the testsuite 'UNREFERENCED' because it is not listed in the runconfig file"]))
        self.assertNotIn("UNREFERENCED", runcfg.testsuites)

        calls = [call('machine1', 0, 'CIResults/tests/results/RENDERCHECK/machine1/0'),
                 call('machine2', 0, 'CIResults/tests/results/RENDERCHECK/machine2/0'),
                 call('machine1', 0, 'CIResults/tests/results/IGT/machine1/0'),
                 call('machine1', 1, 'CIResults/tests/results/IGT/machine1/1'),
                 call('machine2', 0, 'CIResults/tests/results/IGT/machine2/0')]
        mocked_ts_run.return_value.read_results.assert_has_calls(calls, any_order=True)

    def test_init__invalid_testsuite_build(self):
        results = [TestSuiteRunDef(testsuite_build="INVALID", results_format="piglit",
                                   results_format_version=1, machine="machine",
                                   ts_run_id=1, ts_run_path="")]
        msg = "The build named 'INVALID' does not exist"
        self.assertRaisesMessage(ValueError, msg, RunConfigResults, name="RUNCFG",
                                 builds=['INVALID'], results=results)

    def test_init__testsuite_build_not_in_the_list_of_builds(self):
        results = [TestSuiteRunDef(testsuite_build="MISSING", results_format="piglit",
                                   results_format_version=1, machine="machine",
                                   ts_run_id=1, ts_run_path="")]
        msg = "The build named 'MISSING' is not part of the list of builds of the runconfig"
        self.assertRaisesMessage(ValueError, msg, RunConfigResults, name="RUNCFG",
                                 builds=["build1", "build2"], results=results)

    @patch('sys.stderr', new_callable=StringIO)
    def test_init__dual_import_of_a_testsuite_run(self, mocked_stderr):
        call_command('loaddata', 'CIResults/fixtures/RunConfigResults_commit_to_db', verbosity=0)

        results = [TestSuiteRunDef(testsuite_build="build1", results_format="piglit",
                                   results_format_version=1, machine="machine",
                                   ts_run_id=1, ts_run_path="CIResults/tests/results/IGT/machine1/0/results.json.bz2"),
                   TestSuiteRunDef(testsuite_build="build1", results_format="piglit",
                                   results_format_version=1, machine="machine",
                                   ts_run_id=1, ts_run_path="CIResults/tests/results/IGT/machine1/0/results.json.bz2")]

        msg = "Try to import twice the run ID 1 on the runconfig 'RUNCFG' for the machine 'machine'"
        self.assertRaisesMessage(ValueError, msg, RunConfigResults, name="RUNCFG",
                                 builds=["build1"], results=results)

    @patch('os.listdir', return_value=[])
    def __create_commit_to_db_env__(self, mocked_listdir, temporary=False):
        self.__mock_configparser__()

        # Mock a configuration
        cfg = """[CIRESULTS_RUNCONFIG]
        name: RUNCFG
        url: http://test.url.com
        result_url_pattern = http://hello.world/{runconfig}/{machine}/{testsuite_build}/{run_id}/{test}
        environment: my environment
        builds: build1 build2
        tags: tag1 tag2
        temporary: {temporary}

        [Test suite 1]
        build: build1
        format: piglit

        [Test suite 2]
        build: build2
        format: piglit
        """.format(temporary=temporary, runconfig="{runconfig}", machine="{machine}",
                   testsuite_build="{testsuite_build}", run_id="{run_id}", test="{test}")
        self.config.read_string(cfg)
        runcfg = RunConfigResults("")

        # Add results
        self.start_time_run = timezone.make_aware(datetime.datetime.fromtimestamp(0),
                                                  timezone.get_default_timezone())
        self.start_time_test = self.start_time_run + datetime.timedelta(seconds=1)
        for testsuite in ["Test suite 1", "Test suite 2"]:
            for i in range(1, 3):
                results = []
                for t in range(1, 3):
                    results.append(TestsuiteTestResult(name="test{}".format(t),
                                                       status="pass" if t != 2 else "broken",
                                                       command="command",
                                                       stdout="stdout",
                                                       stderr="stderr",
                                                       dmesg="dmesg",
                                                       start_time=self.start_time_test,
                                                       duration=datetime.timedelta(seconds=1)))

                tsr = TestsuiteRunResults(testsuite=runcfg.testsuites[testsuite],
                                          machine_name="machine{}".format(i),
                                          run_id=i, test_results=results,
                                          start_time=self.start_time_run,
                                          duration=datetime.timedelta(seconds=4242))
                runcfg._run_results.append(tsr)

        return runcfg

    @patch('sys.stdout', new_callable=StringIO)
    def test_commit_to_db_machine_public(self, mocked_stdout):
        call_command('loaddata', 'CIResults/fixtures/RunConfigResults_commit_to_db', verbosity=0)
        runcfg = self.__create_commit_to_db_env__()

        # Check the amount of objects before adding anything
        self.assertEqual(Machine.objects.all().count(), 1)
        self.assertEqual(Test.objects.all().count(), 1)
        self.assertEqual(TextStatus.objects.all().count(), 4)
        self.assertEqual(TestsuiteRun.objects.all().count(), 0)
        self.assertEqual(TestResult.objects.all().count(), 0)
        self.assertEqual(KnownFailure.objects.all().count(), 0)
        self.assertEqual(UnknownFailure.objects.all().count(), 0)
        self.assertEqual(RunFilterStatistic.objects.all().count(), 0)

        testsuite1 = TestSuite.objects.get(name="testsuite1")
        testsuite2 = TestSuite.objects.get(name="testsuite2")

        # Add the runconfig to the db
        runcfg.commit_to_db(new_machines_public=True, new_tests_public=False)

        # Check stdout
        stdout_lines = mocked_stdout.getvalue().splitlines()
        self.assertEqual(stdout_lines[0:-4],
                         ['adding 1 missing machine(s)',
                          'adding 1 missing test(s) (testsuite1)',
                          'adding 2 missing test(s) (testsuite2)',
                          'adding 1 missing statuse(s) (testsuite1)',
                          'adding 1 missing statuse(s) (testsuite2)',
                          'adding 4 testsuite runs',
                          'adding 8 test results'])
        err_msg = 'Found 4 test failures (0 filters matched, 4 failures left unmatched) in '
        self.assertTrue(stdout_lines[-4].startswith(err_msg), stdout_lines[-4])

        err_msg = 'Found 0/0 recently-archived filters matching some unknown failures in '
        self.assertTrue(stdout_lines[-1].startswith(err_msg), stdout_lines[-1])

        # Check that the machine and test got created with the correct public attribute
        self.assertTrue(Machine.objects.get(name="machine2").public)
        self.assertFalse(Test.objects.get(name="test2", testsuite=testsuite1).public)
        self.assertFalse(Test.objects.get(name="test1", testsuite=testsuite2).public)
        self.assertFalse(Test.objects.get(name="test2", testsuite=testsuite2).public)

        # Check that the new status is in the database for both testsuites
        self.assertEqual(TextStatus.objects.filter(name="broken").count(), 2)

        # Check that the right amount of objects is found in the db
        self.assertEqual(Machine.objects.all().count(), 2)
        self.assertEqual(Test.objects.all().count(), 4)
        self.assertEqual(TextStatus.objects.all().count(), 6)
        self.assertEqual(TestsuiteRun.objects.all().count(), 4)
        self.assertEqual(TestResult.objects.all().count(), 8)
        self.assertEqual(KnownFailure.objects.all().count(), 0)
        self.assertEqual(UnknownFailure.objects.all().count(), 4)
        self.assertEqual(RunFilterStatistic.objects.all().count(), 0)

    @patch('sys.stdout', new_callable=StringIO)
    def test_commit_to_db_test_public(self, mocked_stdout):
        call_command('loaddata', 'CIResults/fixtures/RunConfigResults_commit_to_db', verbosity=0)
        runcfg = self.__create_commit_to_db_env__()

        testsuite1 = TestSuite.objects.get(name="testsuite1")
        testsuite2 = TestSuite.objects.get(name="testsuite2")

        runcfg.commit_to_db(new_machines_public=False, new_tests_public=True)

        # Check that the machine and test got created with the correct public attribute
        self.assertFalse(Machine.objects.get(name="machine2").public)
        self.assertTrue(Test.objects.get(name="test2", testsuite=testsuite1).public)
        self.assertTrue(Test.objects.get(name="test1", testsuite=testsuite2).public)
        self.assertTrue(Test.objects.get(name="test2", testsuite=testsuite2).public)

        # Check that they all have the first_runconfig set correctly (new tests
        # got the current runconfig, and the old one still has the same value)
        old_runcfg = RunConfig.objects.get(name="OLD_RUNCFG")
        new_runcfg = RunConfig.objects.get(name="RUNCFG")
        for test in Test.objects.all():
            runcfg = (old_runcfg if test.id == 1 else new_runcfg)
            self.assertEqual(test.first_runconfig, runcfg, test)

    @patch('sys.stdout', new_callable=StringIO)
    def test_commit_to_db_test_temporary(self, mocked_stdout):
        call_command('loaddata', 'CIResults/fixtures/RunConfigResults_commit_to_db', verbosity=0)
        runcfg = self.__create_commit_to_db_env__(temporary=True)

        runcfg.commit_to_db(new_machines_public=False, new_tests_public=True)

        # Check that they all have the first_runconfig set correctly, and the new
        # test get None instead
        old_runcfg = RunConfig.objects.get(name="OLD_RUNCFG")
        for test in Test.objects.all():
            runcfg = (old_runcfg if test.id == 1 else None)
            self.assertEqual(test.first_runconfig, runcfg, test)

    def test_commit_to_db__two_builds_of_the_same_component(self):
        call_command('loaddata', 'CIResults/fixtures/RunConfigResults_commit_to_db', verbosity=0)

        component1 = Component.objects.get(name="testsuite1")
        Build.objects.create(name="build3", component=component1)

        runcfg = RunConfigResults(name="RUNCFG", url="http://test.url.com",
                                  result_url_pattern="", builds=["build1", "build3"],
                                  tags=["tag1", "tag2"])

        msg = "ERROR: Two builds (build3 and build1) cannot be from the same component (testsuite1)"
        self.assertRaisesMessage(ValueError, msg, runcfg.commit_to_db)

    @patch('sys.stdout', new_callable=StringIO)
    def test_commit_to_db__add_one_component(self, mocked_stdout):
        call_command('loaddata', 'CIResults/fixtures/RunConfigResults_commit_to_db', verbosity=0)

        # First, create a runconfig with two builds from two different testsuites
        runcfg = RunConfigResults(name="RUNCFG", url="http://test.url.com",
                                  result_url_pattern="", builds=["build1"],
                                  tags=["tag1", "tag2"])
        runcfg.commit_to_db()

        # Add the build2
        runcfg = RunConfigResults(name="RUNCFG", builds=["build2"])
        runcfg.commit_to_db()

        # Verify that the runconfig has been modified to also contain the new build
        runcfg_obj = RunConfig.objects.get(name="RUNCFG")
        self.assertEqual(set([b.id for b in runcfg_obj.builds.all()]), set([1, 2]))

    @patch('sys.stdout', new_callable=StringIO)
    def test_commit_to_db__try_changing_build_of_one_component(self, mocked_stdout):
        call_command('loaddata', 'CIResults/fixtures/RunConfigResults_commit_to_db', verbosity=0)

        testsuite1 = Component.objects.get(name="testsuite1")
        Build.objects.create(name="build3", component=testsuite1)

        # First, create a runconfig with two builds from two different testsuites
        runcfg = RunConfigResults(name="RUNCFG", url="http://test.url.com",
                                  result_url_pattern="", builds=["build1", "build2"],
                                  tags=["tag1", "tag2"])
        runcfg.commit_to_db()

        runcfg = RunConfigResults(name="RUNCFG", builds=["build3"])
        msg = "ERROR: Two builds (build3 and build1) cannot be from the same component (testsuite1)"
        self.assertRaisesMessage(ValueError, msg, runcfg.commit_to_db)
