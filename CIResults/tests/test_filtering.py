from unittest.mock import patch
from django.test import TestCase
from django.db.models import Q

from CIResults.models import Issue, Bug, TestsuiteRun, TestResult, KnownFailure
from CIResults.filtering import UserFiltrableMixin, FilterObjectStr, FilterObjectBool, FilterObject, LegacyParser
from CIResults.filtering import QueryParser, FilterObjectInteger, FilterObjectDateTime, FilterObjectDuration

import datetime
import pytz


class UserFiltrableTestsMixin:
    def test_filter_objects_to_db(self):
        # Abort if the class does not have a model
        if not hasattr(self, 'Model'):
            raise ValueError("The class '{}' does not have a 'Model' attribute".format(self))  # pragma: no cover

        # Abort if the object does not have the filter_objects_to_db attribute
        if not hasattr(self.Model, 'filter_objects_to_db'):
            raise ValueError("The model '{}' does not have a 'filter_objects_to_db "
                             "attribute'".format(self.Model))  # pragma: no cover

        # execute the query with USE_TZ=False to ignore the naive datetime warning
        with self.settings(USE_TZ=False):
            for field_name, db_obj in self.Model.filter_objects_to_db.items():
                filter_name = '{}__exact'.format(db_obj.db_path)
                try:
                    self.Model.objects.filter(**{filter_name: db_obj.test_value})
                except Exception as e:                                                        # pragma: no cover
                    self.fail("Class {}'s field '{}' is not working: {}.".format(self.Model,
                                                                                 field_name,
                                                                                 str(e)))     # pragma: no cover


class BugTests(TestCase, UserFiltrableTestsMixin):
    Model = Bug


class IssueTests(TestCase, UserFiltrableTestsMixin):
    Model = Issue


class TestsuiteRunTests(TestCase, UserFiltrableTestsMixin):
    Model = TestsuiteRun


class TestResultTests(TestCase, UserFiltrableTestsMixin):
    Model = TestResult


class KnownFailureTests(TestCase, UserFiltrableTestsMixin):
    Model = KnownFailure


class QueryParserTests(TestCase):
    def setUp(self):
        self.filter_objects_to_db = {
            "user_abc": FilterObjectInteger('db__abc'),
            "user_def": FilterObjectStr('db__def'),
            "user_ghi": FilterObjectDateTime('db__ghi'),
            "user_jkl": FilterObjectDuration('db__jkl'),
            "user_mno": FilterObjectBool('db__mno'),
            "user_pqr": FilterObjectDateTime('db__pqr'),
        }

    def test_empty_query(self):
        parser = QueryParser(self, "")
        self.assertTrue(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())
        self.assertEqual(parser.error, None)

    def test_invalid_object_name(self):
        parser = QueryParser(self, "hello = 'world'")

        self.assertFalse(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())
        self.assertEqual(parser.error, "The object 'hello' does not exist")

    def test_invalid_syntax(self):
        parser = QueryParser(self, "hello = 'world")

        self.assertFalse(parser.is_valid, parser.error)
        self.assertTrue(parser.is_empty)
        self.assertEqual(parser.q_objects, Q())
        self.assertEqual(parser.error, "Expected ''' at position (1, 15) => 'o = 'world*'.")

    @patch('django.utils.timezone.now',
           return_value=datetime.datetime.strptime('2019-01-01T00:00:05', "%Y-%m-%dT%H:%M:%S"))
    def test_parsing_all_types(self, now_mocked):
        parser = QueryParser(self, "user_abc=123 AND user_def = 'HELLO' AND user_ghi =datetime(2019-02-01) "
                                   "AND user_jkl = duration(00:00:03) AND user_jkl > ago(00:00:05) "
                                   "AND user_mno = TRUE AND user_pqr = NONE")

        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)
        self.assertEqual(parser.q_objects,
                         Q(db__abc__exact=123, db__def__exact='HELLO', db__mno__exact=True,
                           db__ghi__exact=FilterObjectDateTime.parse_value('2019-02-01'),
                           db__jkl__exact=datetime.timedelta(seconds=3),
                           db__jkl__gt=datetime.datetime.strptime('2019-01-01', "%Y-%m-%d"),
                           db__pqr__exact=None))

    def test_integer_lookups(self):
        for lookup, suffix in [('<=', 'lte'), ('>=', 'gte'), ('<', 'lt'), ('>', 'gt'), ('<', 'lt'), ('=', 'exact')]:
            parser = QueryParser(self, "user_abc {} 1234".format(lookup))
            key = "db__abc__{}".format(suffix)
            self.assertEqual(parser.q_objects, Q(**{key: 1234}))

        parser = QueryParser(self, "user_abc IS IN [12, 34]".format(lookup))

        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)
        self.assertEqual(parser.q_objects, Q(db__abc__in=[12, 34]))

    def test_string_lookups(self):
        for lookup, suffix in [('CONTAINS', 'contains'), ('ICONTAINS', 'icontains'),
                               ('MATCHES', 'regex'), ('~=', 'regex'), ('=', 'exact')]:
            parser = QueryParser(self, "user_def {} 'hello'".format(lookup))
            key = "db__def__{}".format(suffix)
            self.assertEqual(parser.q_objects, Q(**{key: "hello"}))

        parser = QueryParser(self, "user_def IS IN ['hello','world']".format(lookup))
        self.assertEqual(parser.q_objects, Q(db__def__in=['hello', 'world']))

    def test_complex_query1(self):
        parser = QueryParser(self, '''(user_abc IS IN ["toto","titi"] AND user_def=datetime(2018-06-23)) OR
                                      ((user_ghi > 456 AND NOT user_def ~= "hello" ) OR user_ghi < 456)''')
        q_filter = (Q(**{'db__abc__in': ['toto', 'titi']}) & Q(**{'db__def__exact':
                    datetime.datetime(2018, 6, 23, 0, 0, tzinfo=pytz.utc)})) | ((Q(**{'db__ghi__gt': 456})
                                                                                & ~Q(**{'db__def__regex': 'hello'})) |
                                                                                Q(**{'db__ghi__lt': 456}))
        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)
        self.assertEqual(parser.q_objects, q_filter)

    def test_complex_query2(self):
        parser = QueryParser(self, '''(user_abc IS IN [2,3,4] OR user_abc IS IN [2,3] )
                                       AND (user_abc <= 1 OR user_abc >= 0)''')
        q_filter = (Q(**{'db__abc__in': [2, 3, 4]}) | Q(**{'db__abc__in': [2, 3]}))\
            & (Q(**{'db__abc__lte': 1}) | Q(**{'db__abc__gte': 0}))

        self.assertTrue(parser.is_valid, parser.error)
        self.assertFalse(parser.is_empty)
        self.assertEqual(parser.q_objects, q_filter)


class LegacyParserTests(TestCase):
    def setUp(self):
        UserFiltrableMixin.filter_objects_to_db = {
            "user_abc": FilterObjectStr('db__abc'),
            "user_def": FilterObjectStr('db__def'),
            "user_ghi": FilterObjectStr('db__ghi'),
            "user_jkl": FilterObjectBool('db__jkl'),
            "user_mno": FilterObjectDuration('db__mno'),
        }

    def test_no_filters(self):
        parser = LegacyParser(UserFiltrableMixin)
        self.assertEqual(parser.query, "")

    def test_valid_filters(self):
        parser = LegacyParser(UserFiltrableMixin,
                              only__user_abc__in=['toto', 'int(1234.3)'],
                              only__user_def__exact='datetime(2018-06-23)',
                              only__user_ghi__gt=['int(456)'],
                              only__user_jkl__exact='bool(1)',
                              only__user_mno__exact='duration(00:00:03)',
                              exclude__user_def__regex='str(hello)')
        self.assertEqual(parser.query,
                         "user_abc IS IN ['toto', 1234.3] AND user_def = datetime(2018-06-23) AND user_ghi > 456 "
                         "AND user_jkl = TRUE AND user_mno = duration(00:00:03) AND NOT (user_def ~= 'hello')")

    def test_regex_aggregation(self):
        parser = LegacyParser(UserFiltrableMixin, only__user_abc__regex=['toto', 'tata', 'titi'])
        self.assertEqual(parser.query, "user_abc ~= '(toto|tata|titi)'")

    def test_invalid_formats(self):
        parser = LegacyParser(UserFiltrableMixin, balbla='ghujfdk', oops__user_abc__in=12,
                              only__invalid__in=13, only__user_abc__toto=14)
        self.assertEqual(parser.query, "")


class UserFiltrableMixinTests(TestCase):
    def test_old_style(self):
        queryset = TestResult.from_user_filters(only__status_name__exact='pass').objects
        self.assertIn('WHERE "CIResults_textstatus"."name" = pass', str(queryset.query))

    def test_new_style(self):
        queryset = TestResult.from_user_filters(query=['status_name = "pass"']).objects
        self.assertIn('WHERE "CIResults_textstatus"."name" = pass', str(queryset.query))


class FilterObjectTests(TestCase):
    def test_empty_description(self):
        self.assertEqual(FilterObject("").description, "<no description yet>")

    def test_with_description(self):
        self.assertEqual(FilterObject("", "My description").description, "My description")


class FilterObjectDurationTests(TestCase):
    def test_invalid_value(self):
        with self.assertRaisesRegexp(ValueError, "The value '1 month' does not represent a duration"):
            FilterObjectDuration.parse_value('1 month')
