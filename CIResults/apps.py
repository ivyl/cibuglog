from django.apps import AppConfig
from datetime import timedelta
import traceback
import threading
import atexit
import math
import time
import sys


class poll_bugtrackers(threading.Thread):
    def __init__(self, polling_period):
        super().__init__(daemon=True)

        self._polling_period = polling_period
        self._stop_sig = threading.Event()

    def __del__(self):
        self.stop()

    def stop(self):
        self._stop_sig.set()
        self.join()

    def __interuptable_sleep__(self, seconds, step_seconds=.1):
        for i in range(math.ceil(seconds / step_seconds)):
            # Exit early if we are trying to reset the process
            if self._stop_sig.is_set():
                return

            # Sleep one second otherwise
            time.sleep(step_seconds)

    def run(self):
        from .models import BugTracker, Issue

        # Wait a few seconds before starting, to give time to boot
        self.__interuptable_sleep__(5)

        while not self._stop_sig.is_set():
            start_time = time.time()

            for tracker in BugTracker.objects.all():
                try:
                    # Exit immediately if we have been signalled to
                    if self._stop_sig.is_set():
                        return

                    # Collect all the bugs to be updated
                    # TODO: Verify how many SQL requests this generates
                    bugs_in_issues = set()
                    for issue in Issue.objects.all().prefetch_related('bugs__tracker'):
                        for bug in issue.bugs.all():
                            if bug.tracker == tracker:
                                bugs_in_issues.add(bug)
                    open_bugs = tracker.open_bugs()

                    # Update the bugs
                    tracker.poll_all(stop_event=self._stop_sig, bugs=(open_bugs | bugs_in_issues))
                except Exception:
                    traceback.print_exc()

            # Now compute how long we have to wait until the next poll
            s = self._polling_period.total_seconds() - (time.time() - start_time)
            self.__interuptable_sleep__(s)


class CiresultsConfig(AppConfig):
    name = 'CIResults'

    def ready(self):
        # If we are running the server, kick off a background task that
        # will poll the bug reports
        if len(sys.argv) > 1 and sys.argv[1] == 'runserver':
            self.poll_thread = poll_bugtrackers(timedelta(minutes=10))
            self.poll_thread.start()

            # make sure the stop function is called at the end!
            atexit.register(self.__stop_polling_trackers__)

    def __stop_polling_trackers__(self):
        self.poll_thread.stop()
