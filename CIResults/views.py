from django.template.loader import render_to_string
from django.contrib import messages
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.core.paginator import Paginator
from django.core.validators import validate_email, URLValidator
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from django.views import View
from django.views.generic import DetailView, ListView
from django.views.generic.edit import UpdateView, FormView
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.db import transaction
from django.db.models import Prefetch
from django.urls import reverse
from django.utils import timezone
from django.utils.html import escape
from django.utils.timesince import timesince
from django import forms

from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework import status, viewsets

from collections import namedtuple, defaultdict

from .serializers import IssueFilterSerializer, RunConfigSerializer, RunConfigDiffSerializer, ComponentSerializer
from .serializers import BuildSerializer, TestSerializer, BugTrackerAccountSerializer, KnownIssuesSerializer
from .models import BugTracker, Bug, Component, Build, Test, Machine, RunConfigTag, RunConfig, TestSuite, TextStatus
from .models import TestResult, IssueFilter, IssueFilterAssociated, Issue, KnownFailure, UnknownFailure, MachineTag
from .models import BugTrackerAccount, BugComment

from .metrics import metrics_issues_over_time, metrics_bugs_over_time, metrics_issues_ttr, metrics_bugs_ttr
from .metrics import metrics_open_issues_age, metrics_open_bugs_age, metrics_failure_filing_delay
from .metrics import metrics_testresult_statuses_stats, metrics_testresult_machines_stats
from .metrics import metrics_testresult_tests_stats, metrics_testresult_issues_stats
from .metrics import metrics_knownfailure_statuses_stats, metrics_knownfailure_machines_stats
from .metrics import metrics_knownfailure_tests_stats, metrics_knownfailure_issues_stats
from .metrics import MetricPassRateHistory, MetricRuntimeHistory

from .forms import TestMassRenameForm

from datetime import timedelta

import traceback
import json
import re


def index(request, **kwargs):
    public = kwargs.get("public", False)

    uf = UnknownFailure.objects.prefetch_related('result', 'result__test',
                                                 'result__test__testsuite',
                                                 'result__ts_run',
                                                 'result__ts_run__machine',
                                                 'result__ts_run__runconfig',
                                                 'result__ts_run__testsuite',
                                                 'result__status__testsuite',
                                                 'result__status',
                                                 'matched_archived_ifas',
                                                 'matched_archived_ifas__issue__bugs')
    uf = uf.filter(result__ts_run__runconfig__temporary=False).order_by('id')

    ifas_query = IssueFilterAssociated.objects.all()
    issues = Issue.objects.filter(archived_on=None, expected=False).prefetch_related(Prefetch('filters',
                                                                                              to_attr='ifas_cached',
                                                                                              queryset=ifas_query),
                                                                                     "ifas_cached__filter",
                                                                                     "bugs__tracker",
                                                                                     "bugs").order_by('-id')
    suppressed_tests = Test.objects.prefetch_related('testsuite').filter(vetted_on=None).exclude(first_runconfig=None)
    context = {
            "trackers": BugTracker.objects.all(),
            "issues": issues,
            "suppressed_tests": suppressed_tests.prefetch_related('first_runconfig'),
            "suppressed_machines": Machine.objects.filter(vetted_on=None),
            "suppressed_statuses": TextStatus.objects.filter(vetted_on=None),
            "unknown_failures": uf,
            "public": public,
        }
    return render(request, 'CIResults/index.html', context)


class IssueListView(ListView):
    model = Issue

    @property
    def public(self):
        return self.kwargs.get('public', False)

    def get_queryset(self):
        requested_filters = self.request.GET.copy()
        self.query = Issue.from_user_filters(**requested_filters)
        if not self.query.is_valid:
            messages.error(self.request, "Filtering error: " + self.query.error)

        issues = Issue.from_user_filters(**requested_filters).objects

        # Prefetch all the necessary things before returning the list
        ifas_query = IssueFilterAssociated.objects.all()
        return issues.prefetch_related(Prefetch('filters', to_attr='ifas_cached',
                                                queryset=ifas_query),
                                       "ifas_cached__filter",
                                       "bugs__tracker",
                                       "bugs").order_by('-id')

    def get_context_data(self):
        context = super().get_context_data()
        context["trackers"] = BugTracker.objects.all()
        context['public'] = self.public
        context['issues'] = context.pop('object_list')
        context['filters_model'] = Issue
        context['query'] = self.query
        return context


def api_metrics(request):
    metrics = {
        "issues": Issue.objects.filter(archived_on=None).count(),
        "machine": Machine.objects.all().count(),
        "suppressed_tests": Test.objects.filter(vetted_on=None).exclude(first_runconfig=None).count(),
        "suppressed_machines": Machine.objects.filter(vetted_on=None).count(),
        "tests": Test.objects.all().count(),
        "unknown_failures": UnknownFailure.objects.filter(result__ts_run__runconfig__temporary=False).count(),
        "version": 1,
    }

    return JsonResponse(metrics)


def metrics_generic_history(request, History, history_kwargs, template_path, **kwargs):
    public = kwargs.get("public", False)

    # convert the user filters to a normal dictionary to prevent issues when
    # inserting new values
    requested_filters = dict(request.GET.copy())

    # Default to a sane query if no query has been made
    query = History.filtering_model.from_user_filters(**requested_filters)
    if query.is_valid and query.is_empty:
        runconfigs = RunConfig.objects.filter(temporary=False).order_by('-id')[:10]
        runconfigs_str = ','.join(["'{}'".format(r.name) for r in runconfigs])
        requested_filters['query'] = "runconfig_name IS IN [{}]".format(runconfigs_str)
    elif not query.is_valid:
        messages.error(request, "Filtering error: " + query.error)

    context = {
        "public": public,
        'history': History(requested_filters, **history_kwargs),
    }
    return render(request, template_path, context)


def metrics_passrate_history(request, **kwargs):
    return metrics_generic_history(request, MetricPassRateHistory, {}, 'CIResults/metrics_passrate.html', **kwargs)


def metrics_runtime_history(request, **kwargs):
    return metrics_generic_history(request, MetricRuntimeHistory,
                                   {"average_per_machine": request.GET.get('average', '0') == '1'},
                                   'CIResults/metrics_runtime.html', **kwargs)


def metrics_overview(request, **kwargs):
    public = kwargs.get("public", False)

    context = {
            "public": public,
            "issues_weekly":  metrics_issues_over_time().stats(),
            "issues_ttr": metrics_issues_ttr().stats,
            "issues_age": metrics_open_issues_age().stats,
            "failure_filing_delay": metrics_failure_filing_delay().stats,

            "bugs_weekly":  metrics_bugs_over_time().stats(),
            "bugs_ttr": metrics_bugs_ttr().stats,
            "bugs_age": metrics_open_bugs_age().stats,
        }
    return render(request, 'CIResults/metrics_overview.html', context)


def metrics_bugs(request, **kwargs):
    public = kwargs.get("public", False)

    requested_filters = request.GET.copy()
    query = Bug.from_user_filters(**requested_filters)
    if not query.is_valid:
        messages.error(request, "Filtering error: " + query.error)

    bugs_weekly = metrics_bugs_over_time(user_filters=requested_filters).stats()
    bugs_ttr = metrics_bugs_ttr(user_filters=requested_filters).stats
    bugs_age = metrics_open_bugs_age(user_filters=requested_filters).stats

    context = {
            "public": public,

            "bugs_weekly":  bugs_weekly,
            "bugs_ttr": bugs_ttr,
            "bugs_age": bugs_age,

            'filters_model': Bug,
            'query': query,
        }
    return render(request, 'CIResults/metrics_bugs.html', context)


def open_bugs(request, **kwargs):
    public = kwargs.get("public", False)

    # Parse the user request
    user_query = Bug.from_user_filters(**request.GET.copy())
    if not user_query.is_valid:
        messages.error(request, "Filtering error: " + user_query.error)

    # If the user request is empty, just select everything
    selected_bugs = Bug.objects.all() if user_query.is_empty else user_query.objects
    selected_bugs = selected_bugs.prefetch_related('tracker', 'assignee__person', "creator__person")

    referenced_bugs = set([b['id'] for b in Bug.objects.filter(issue__archived_on=None).values('id')])
    all_followed_and_open_bugs = set([b for b in selected_bugs if b.is_open and (b.id in referenced_bugs or
                                                                                 b.component in b.tracker.components_followed_list)])  # noqa
    all_comments = BugComment.objects.filter(bug__in=selected_bugs).prefetch_related('bug',
                                                                                     'bug__tracker',
                                                                                     "account",
                                                                                     "account__person")

    # HACK: Preload all the comments and assign them to their respective bugs
    comments = defaultdict(list)
    for comment in all_comments:
        comments[(comment.bug.id, comment.bug.tracker.id)].append(comment)
    for bug in all_followed_and_open_bugs:
        bug.comments_cached = comments[(bug.id, bug.tracker.id)]

    # Now, classify the urgency
    overdue = list()
    close = list()
    other = list()
    for bug in sorted(all_followed_and_open_bugs, key=lambda b: b.effective_priority, reverse=True):
        time_left = bug.SLA_remaining_time
        if time_left < timedelta(seconds=0):
            overdue.append(bug)
        elif time_left < timedelta(days=7):
            close.append(bug)
        else:
            other.insert(-1, bug)

    # Create a store for involvement statistics
    DeveloperStatistics = namedtuple('DeveloperStatistics', ('bugs_created', 'bugs_involved',
                                                             'bugs_assigned', 'bugs_still_open',
                                                             'bugs_closed', 'all_comments'))
    devs = dict()
    for dev in sorted(BugTrackerAccount.objects.filter(is_developer=True), key=lambda a: str(a)):
        devs[dev] = DeveloperStatistics(bugs_created=set(), bugs_assigned=set(), bugs_involved=set(),
                                        bugs_still_open=set(), bugs_closed=set(), all_comments=set())

    # Get statistics on who created bugs
    days_ago_30 = timezone.now() - timedelta(days=30)
    for bug in selected_bugs:
        if bug.created is not None and bug.created > days_ago_30:
            if bug.creator is not None and bug.creator.is_developer:
                devs[bug.creator].bugs_created.add(bug)

    # Count how many bugs are assigned
    for bug in all_followed_and_open_bugs:
        if bug.assignee in devs:
            devs[bug.assignee].bugs_assigned.add(bug)

    # Go through all comments and assign them to their author
    for comment in all_comments:
        # Ignore comments older than 30 days and from non-dev accounts
        if comment.created_on <= days_ago_30 or not comment.account.is_developer:
            continue

        dev = devs[comment.account]
        dev.all_comments.add(comment)
        dev.bugs_involved.add(comment.bug)
        if comment.bug.is_open:
            dev.bugs_still_open.add(comment.bug)
        else:
            dev.bugs_closed.add(comment.bug)

    # Remove the developers who did not interact in the past 30 days
    for dev in list(devs.keys()):
        stats = devs[dev]
        if len(stats.bugs_created) == 0 and len(stats.bugs_assigned) == 0 and len(stats.all_comments) == 0:
            del devs[dev]

    context = {
        "trackers": BugTracker.objects.all(),
        "all_open_bugs": all_followed_and_open_bugs,
        "bugs_overdue": overdue,
        "bugs_close_to_deadline": close,
        "bugs_other": other,
        "developers": devs,
        "public": public,

        'filters_model': Bug,
        'query': user_query,
    }
    return render(request, 'CIResults/open_bugs.html', context)


class HistoryView(View):
    def __add_info_URL__(self, info, title, url, index=None):
        if url is None or len(url) == 0:
            return

        try:
            # Verify that the url really is a URL before marking the string as safe
            URLValidator()(url)
            self.__add_info_line__(info, title, mark_safe("<a href='{}'>{}</a>".format(url, escape(url))),
                                   index=index)
        except ValidationError:
            pass

    def __add_info_line__(self, info, title, objects, placeholder="<None>", index=None):
        if objects is None:
            return

        if len(objects) > 0:
            if isinstance(objects, list):
                if len(objects) > 5:
                    title = "{} ({})".format(title, len(objects))

                value = mark_safe(", ".join(objects))
            else:
                value = objects

            new_line = (escape(title), value)
        else:
            new_line = (escape(title), escape(placeholder))

        if index is not None:
            info.insert(index, new_line)
        else:
            info.append(new_line)

    def __paginate__(self, request, queryset, default_size=100):
        page = request.GET.get('page')
        page_size = request.GET.get('page_size')

        paginator = Paginator(queryset, page_size if page_size is not None else default_size)
        return paginator.get_page(page)

    def __failure_history_page__(self, request, object, info, filter_kwargs=None,
                                 chart_all_failures=False):
        f_query = KnownFailure.objects.prefetch_related('result__test',
                                                        'result__test__testsuite',
                                                        'result__ts_run',
                                                        'result__ts_run__machine',
                                                        'result__ts_run__runconfig',
                                                        'result__ts_run__testsuite',
                                                        'result__status',
                                                        'result__status__testsuite',
                                                        'matched_ifa',
                                                        'matched_ifa__issue')
        failures = f_query.order_by("-id").filter(result__ts_run__runconfig__temporary=False,
                                                  **filter_kwargs)

        # Get recent failures for the metrics. Stop at the first result older than a month
        if chart_all_failures:
            recent_failures = failures
        else:
            recent_failures = list()
            month_ago = timezone.now() - timedelta(days=30)
            for failure in failures:
                if failure.result.ts_run.runconfig.added_on > month_ago:
                    recent_failures.append(failure)
                else:
                    break

        # Render the page
        context = {
            "object": object,
            "failures": self.__paginate__(request, failures),
            "chart_all_failures": chart_all_failures,
            "statuses_chart": metrics_knownfailure_statuses_stats(recent_failures).stats(),
            "machines_chart": metrics_knownfailure_machines_stats(recent_failures).stats(),
            "tests_chart": metrics_knownfailure_tests_stats(recent_failures).stats(),
            "issues_chart": metrics_knownfailure_issues_stats(recent_failures).stats(),
            "info": info,
        }
        return render(request, 'CIResults/history.html', context)

    def __result_history_page__(self, request, result, info):
        r_query = TestResult.objects.prefetch_related('test', 'test__testsuite',
                                                      'ts_run', 'ts_run__machine',
                                                      'ts_run__runconfig',
                                                      'ts_run__testsuite',
                                                      'status', 'status__testsuite')
        args = {"ts_run__runconfig__in": result.ts_run.runconfig.runcfg_history,
                "ts_run__machine": result.ts_run.machine,
                "test": result.test}
        result_hist = r_query.order_by("-id").filter(ts_run__runconfig__temporary=False,
                                                     **args)

        # Given that all the results are from runs belonging to the result's
        # runcfg history, we can just copy the orginal history to all the
        # results, saving a lot of database requests
        for r in result_hist:
            r.ts_run.runconfig.runcfg_history = result.ts_run.runconfig.runcfg_history

        # Render the page
        context = {
            "object": result,
            "failures": None,
            "result_hist": self.__paginate__(request, result_hist),
            "info": info,
        }
        return render(request, 'CIResults/history.html', context)


class IssueView(HistoryView):
    ParsedParams = namedtuple('ParsedParams',
                              'bugs bugs_form description error email email_valid filters')

    def __parse_params__(self, params):
        # Keep track of all errors, only commit if no error occured
        error = False

        # Parse the bugs
        bugs = []
        bugs_form = []
        for param in params:
            if param.startswith("bug_id_"):
                # Check the bug_id
                bug_id = params[param]
                if len(bug_id) == 0:
                    continue

                # Check the tracker
                suffix = param[7:]
                tracker_pk = params.get("bug_tracker_{}".format(suffix), -1)
                try:
                    tracker_pk = int(tracker_pk)
                    tracker = BugTracker.objects.get(pk=tracker_pk)
                    tracker_valid = True

                    # Check the bug
                    try:
                        bug = Bug.objects.get(tracker=tracker, bug_id=bug_id)
                        bugs.append(bug)
                        bug_valid = True
                    except Exception:
                        # The bug does not exist, try to create it
                        try:
                            bug = Bug(tracker=tracker, bug_id=bug_id)
                            bug.poll()
                            bugs.append(bug)
                            bug_valid = True
                        except Exception:
                            error = True
                            bug_valid = False
                            traceback.print_exc()
                except Exception:
                    error = True
                    tracker_valid = False
                    bug_valid = False
                    traceback.print_exc()

                # Keep the information about the bugs, in case we need to
                # use it again to re-draw the page
                bugs_form.append({"bug_id": bug_id, "tracker_id": tracker_pk,
                                  "bug_valid": bug_valid,
                                  "tracker_valid": tracker_valid})

        # Parse the filters
        filters = []
        for param in params:
            if param.startswith("filter_"):
                try:
                    pk = int(params[param])
                    filters.append(IssueFilter.objects.get(pk=pk))
                except Exception:
                    error = True
                    continue

        # Check the email address
        filer_email = params.get("email")  # TODO: Read the current user's email
        if filer_email is not None:
            try:
                validate_email(filer_email)
                email_valid = True
            except forms.ValidationError:
                error = True
                email_valid = False
        else:
            # The email has not been set yet, so consider it as valid
            filer_email = ""
            email_valid = True

        # Get the description
        description = params.get("description", "")

        return self.ParsedParams(bugs=bugs, bugs_form=bugs_form,
                                 description=description, error=error,
                                 email=filer_email, email_valid=email_valid,
                                 filters=filters)

    def __fetch_from_db__(self, pk):
        issue = get_object_or_404(Issue, pk=pk)

        bugs = issue.bugs.all()
        bugs_form = []
        for bug in bugs:
            bugs_form.append({"bug_id": bug.bug_id, "tracker_id": bug.tracker.id,
                             "bug_valid": True, "tracker_valid": True})

        filters_assoc = IssueFilterAssociated.objects.filter(deleted_on=None, issue=issue)
        filters = [e.filter for e in filters_assoc]

        return self.ParsedParams(bugs=bugs, bugs_form=bugs_form,
                                 description=issue.description, error=False,
                                 email=issue.filer, email_valid=True,
                                 filters=filters)

    @transaction.atomic
    def __save__(self, pk, d):
        if d.error:
            raise ValueError("The data provided contained error")

        if len(d.filters) == 0:
            raise ValueError("No filters found")

        if len(d.bugs) == 0:
            raise ValueError("No bugs found")

        # Try to find the issue to edit, if applicable
        try:
            issue = Issue.objects.get(id=pk)

            # Update the fields
            issue.filer = d.email
            issue.description = d.description
            issue.save()
        except Issue.DoesNotExist:
            issue = Issue.objects.create(filer=d.email, description=d.description)

        # Add the bugs and filters that were set
        issue.set_bugs(d.bugs)
        issue.set_filters(d.filters)

    def __show_page__(self, request, pk, d):
        # Pre-allocate a line for the bug
        if len(d.bugs_form) == 0:
            d.bugs_form.append({"bug_id": "", "tracker_id": -1,
                                "bug_valid": True, "tracker_valid": True})

        # find out whether we are creating or editing an issue
        if pk is None:
            save_url = reverse('CIResults-issue', kwargs={"action": 'create'})
            issue_title = "New Issue"
        else:
            save_url = reverse('CIResults-issue', kwargs={"action": "edit",
                                                          "pk": pk})
            issue_title = "Edit Issue {}".format(pk)

        filters = list(IssueFilter.objects.filter(hidden=False).order_by("-id")[:20])
        for f in d.filters:
            if f not in filters:
                filters.append(f)

        email = d.email
        if request.user.is_authenticated and (email is None or len(email) == 0):
            email = request.user.email

        # Get the list of tests referenced by this issue, then add all the ones
        # that have an associated first runconfig
        tests = set()
        for f in d.filters:
            tests.update(f.tests.all())
        tests.update(Test.objects.prefetch_related("testsuite").exclude(first_runconfig=None))

        # Render the page
        context = {
            "issue_id": pk,
            "issue_title": issue_title,
            "tags": RunConfigTag.objects.all(),
            "machine_tags": MachineTag.objects.all(),
            "machines": Machine.objects.all(),
            "tests": tests,
            "statuses": TextStatus.objects.prefetch_related("testsuite").all(),
            "trackers": BugTracker.objects.all(),
            "filters": filters,
            "field_bugs": d.bugs_form,  # This data may not be valid or commited yet
            "field_filters": d.filters,
            "field_email": email,
            "field_email_valid": d.email_valid,
            "field_description": d.description,
            "save_url": save_url
        }
        return render(request, 'CIResults/issue.html', context)

    def __edit_issue__(self, request, action, pk, params):
        if pk is not None and len(params) == 0:
            # We are editing an issue and have not provided updates for it
            d = self.__fetch_from_db__(pk)
            return self.__show_page__(request, pk, d)
        else:
            # In any other case, just display what we have, or default values
            d = self.__parse_params__(params)

            # Save, if possible. Otherwise, just show the page again
            try:
                self.__save__(pk, d)
                return redirect('CIResults-index')
            except ValueError:
                return self.__show_page__(request, pk, d)

    def __history__(self, request, pk):
        issue = get_object_or_404(Issue, pk=pk)

        # get the list of active and past filters
        ifa_deleted = IssueFilterAssociated.objects.filter(issue=issue).exclude(deleted_on=None).order_by('-deleted_on')
        active = [render_to_string("CIResults/basic/ifa.html", {"ifa": ifa}).strip()
                  for ifa in IssueFilterAssociated.objects.filter(issue=issue, deleted_on=None).order_by('-added_on')]
        deleted = [render_to_string("CIResults/basic/ifa.html", {"ifa": ifa}).strip() for ifa in ifa_deleted]

        bugs = [render_to_string("CIResults/basic/bug.html", {"bug": b}).strip() for b in issue.bugs.all()]
        issue_url = reverse('CIResults-issue', kwargs={'action': 'edit', "pk": issue.id})
        info = [
            ("Issue {}".format(issue.id), mark_safe("<a href='{}'>Edit</a>".format(issue_url))),
            ("Bugs associated", mark_safe(", ".join(bugs))),
            ("Active filters", mark_safe(", ".join([s for s in active]))),
            ("Past filters", mark_safe(", ".join([s for s in deleted]))),
            ("Issue filer", issue.filer),
        ]
        if len(issue.description) > 0:
            info.append(("Comments", issue.description))

        # Render the page
        return self.__failure_history_page__(request, issue, info, {"matched_ifa__issue": issue})

    def __route_request__(self, request, kwargs, params):
        post_actions = ["archive", "restore", "hide", "show"]

        action = kwargs['action']
        if action in post_actions:
            if request.method != 'POST':
                raise ValidationError("The action '{}' is unsupported in a {} request".format(action, request.method))
            issue = get_object_or_404(Issue, pk=kwargs.get('pk'))
            getattr(issue, action)()
            return redirect(request.META.get('HTTP_REFERER', 'CIResults-index'))
        elif action == "history":
            return self.__history__(request, kwargs['pk'])
        else:
            # Action == edit or create
            return self.__edit_issue__(request, action,
                                       kwargs.get('pk'), params)

    def get(self, request, *args, **kwargs):
        return self.__route_request__(request, kwargs, request.GET)

    def post(self, request, *args, **kwargs):
        return self.__route_request__(request, kwargs, request.POST)


class IssueFilterAssociatedView(HistoryView):
    def __history__(self, request, pk):
        ifa = get_object_or_404(IssueFilterAssociated, pk=pk)

        info = [
            ("Filter", mark_safe(render_to_string("CIResults/basic/filter.html", {"filter": ifa.filter}).strip())),
            ("Issue", mark_safe(render_to_string("CIResults/basic/issue.html", {"issue": ifa.issue}).strip())),
            ("Is Active?", str(ifa.active)),
        ]
        if not ifa.active:
            info.extend([
                ("Activity period", timesince(ifa.added_on, ifa.deleted_on)),
                ("Creation date", "{} ({})".format(ifa.added_on.date(), naturaltime(ifa.added_on))),
                ("Deletion date", "{} ({})".format(ifa.deleted_on.date(), naturaltime(ifa.deleted_on))),
            ])
        else:
            info.extend([
                ("Creation date", "{} ({})".format(ifa.added_on.date(), naturaltime(ifa.added_on))),
            ])

        # Render the page
        return self.__failure_history_page__(request, ifa, info, {"matched_ifa": ifa})

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])


class REST_IssueFilterViewSet(viewsets.ModelViewSet):
    queryset = IssueFilter.objects.all().order_by('-id')
    serializer_class = IssueFilterSerializer

    def __check_list__(self, request_data, field, field_name, db_class, errors):
        objects = set(request_data.get(field, []))

        objects_db = dict()
        for obj in db_class.objects.filter(id__in=objects):
            objects_db[obj.id] = obj

        if len(objects) != len(objects_db):
            errors.append("At least one {} does not exist".format(field_name))

        return objects, objects_db

    def __get_or_None__(self, klass, field, request_dict, errors):
        obj = None
        if field in request_dict:
            obj_id = request_dict[field]

            # Do not consider empty strings as meaning a valid value
            if isinstance(obj_id, str) and len(obj_id) == 0:
                return None

            # Convert the id to an int or fail
            try:
                obj_id = int(obj_id)
            except Exception:
                errors.append("The field '{}' needs to be an integer".format(field))
                return None

            # Try getting the object
            obj = klass.objects.filter(id=obj_id).first()
            if obj is None:
                errors.append("The object referenced by '{}' does not exist".format(field))

        return obj

    @transaction.atomic
    def create(self, request):
        request_dict = json.loads(request.body.decode())

        errors = []
        if len(request_dict.get('description', '')) == 0:
            errors.append("The field 'description' cannot be empty")

        # Check if the filter should replace another one
        edit_filter = self.__get_or_None__(IssueFilter, 'edit_filter',
                                           request_dict, errors)
        edit_issue = self.__get_or_None__(Issue, 'edit_issue',
                                          request_dict, errors)

        # Check that all the tags, machines, tests, and statuses are present
        tags, tags_db = self.__check_list__(request_dict, "tags", "tag", RunConfigTag, errors)
        machine_tags, machine_tags_db = self.__check_list__(request_dict, "machine_tags", "machine tag",
                                                            MachineTag, errors)
        machines, machines_db = self.__check_list__(request_dict, "machines", "machine", Machine, errors)
        tests, tests_db = self.__check_list__(request_dict, "tests", "test", Test, errors)
        statuses, statuses_db = self.__check_list__(request_dict, "statuses", "status", TextStatus, errors)

        # Check the regular expressions
        for field in ['stdout_regex', 'stderr_regex', 'dmesg_regex']:
            try:
                re.compile(request_dict.get(field, ""))
            except Exception:
                errors.append("The field '{}' does not contain a valid regular expression".format(field))

        # Create the object or fail depending on whether we got errors or not
        if len(errors) == 0:
            filter = IssueFilter.objects.create(description=request_dict.get('description'),
                                                stdout_regex=request_dict.get('stdout_regex', ""),
                                                stderr_regex=request_dict.get('stderr_regex', ""),
                                                dmesg_regex=request_dict.get('dmesg_regex', ""))

            filter.tags.add(*tags_db)
            filter.machines.add(*machines_db)
            filter.machine_tags.add(*machine_tags_db)
            filter.tests.add(*tests_db)
            filter.statuses.add(*statuses_db)

            # If this filter is supposed to replace another filter
            if edit_filter is not None:
                if edit_issue is not None:
                    edit_issue.replace_filter(edit_filter, filter)
                else:
                    edit_filter.replace(filter)

            serializer = IssueFilterSerializer(filter)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)


class IssueFilterView(HistoryView):
    def __history__(self, request, pk):
        filter = get_object_or_404(IssueFilter, pk=pk)

        tags = [render_to_string("CIResults/basic/tag.html", {"tag": t}).strip() for t in filter.tags.all()]
        machine_tags = [render_to_string("CIResults/basic/machine_tag.html", {"machine_tag": m}).strip()
                        for m in filter.machine_tags.all()]
        machines = [render_to_string("CIResults/basic/machine.html", {"machine": m}).strip()
                    for m in filter.machines.all()]
        tests = [render_to_string("CIResults/basic/test.html", {"test": t}).strip() for t in filter.tests.all()]
        statuses = [render_to_string("CIResults/basic/textstatus.html", {"status": s}).strip()
                    for s in filter.statuses.all()]

        info = [
            ("Description", filter.description),
            ("Added on", "{} ({})".format(filter.added_on.date(), naturaltime(filter.added_on))),
            ("Marked hidden", filter.hidden),
        ]

        self.__add_info_line__(info, "Runconfig tags", tags, "<All tags>")
        self.__add_info_line__(info, "Machine tags", machine_tags, "<All machine tags>")
        self.__add_info_line__(info, "Machines", machines, "<All machines>")
        self.__add_info_line__(info, "Tests", tests, "<All tests>")
        self.__add_info_line__(info, "Statuses", statuses, "<All statuses>")

        self.__add_info_line__(info, "Standard output regex", filter.stdout_regex, "<All outputs>")
        self.__add_info_line__(info, "Standard error regex", filter.stderr_regex, "<All outputs>")
        self.__add_info_line__(info, "Dmesg output regex", filter.dmesg_regex, "<All outputs>")

        # Render the page
        return self.__failure_history_page__(request, filter, info, {"matched_ifa__filter": filter})

    def __update_stats__(self, stats, result):
        for tag in result.ts_run.runconfig.tags.all():
            stats['tags'].add((tag.id, str(tag)))
        stats['machines'].add((result.ts_run.machine.id, str(result.ts_run.machine)))
        if len(result.ts_run.machine.tags_cached) == 0:
            stats['tagless_machines'].add((result.ts_run.machine.id, str(result.ts_run.machine)))
        else:
            for tag in result.ts_run.machine.tags_cached:
                stats['machine_tags'].add((tag.id, str(tag)))
        stats['tests'].add((result.test.id, str(result.test)))
        stats['statuses'].add((result.status.id, str(result.status)))

    def __check_regexp__(self, params, field, errors):
        try:
            regexp = params.get(field, "")
            re.compile(regexp)
            return True
        except re.error as e:
            errors.append({"field": field, "msg": str(e)})
            return False

    def __stats__(self, request):
        stats = {
            "matched": {
                "tags": set(),
                "machine_tags": set(),
                "machines": set(),
                "tagless_machines": set(),
                "tests": set(),
                "statuses": set()
            },
            "errors": [
            ],
        }

        # Read the parameters and perform some input validation
        params = json.loads(request.body.decode())
        self.__check_regexp__(params, 'stdout_regex', stats['errors'])
        self.__check_regexp__(params, 'stderr_regex', stats['errors'])
        self.__check_regexp__(params, 'dmesg_regex', stats['errors'])

        # Check the filter
        if len(stats['errors']) == 0:
            filter = IssueFilter(description=params.get('description'),
                                 stdout_regex=params.get('stdout_regex', ""),
                                 stderr_regex=params.get('stderr_regex', ""),
                                 dmesg_regex=params.get('dmesg_regex', ""))
            filter.tags_ids_cached = set(params.get("tags", []))
            filter.__machines_cached__ = set(Machine.objects.filter(id__in=params.get("machines", [])))
            filter.__machine_tags_cached__ = set(MachineTag.objects.filter(id__in=params.get("machine_tags", [])))
            filter.tests_ids_cached = set(params.get("tests", []))
            filter.statuses_ids_cached = set(params.get("statuses", []))

            # Check which failures are matched by this filter
            req = UnknownFailure.objects.prefetch_related('result',
                                                          'result__ts_run__runconfig__tags',
                                                          'result__ts_run__machine',
                                                          'result__test',
                                                          'result__status')
            req = req.filter(result__ts_run__runconfig__temporary=False)

            matched_failures = 0
            for failure in req:
                if filter.matches(failure.result):
                    matched_failures += 1
                    self.__update_stats__(stats['matched'], failure.result)

            for key in stats['matched']:
                stats['matched'][key] = sorted(stats['matched'][key])
            stats['matched']['failure_total'] = req.count()
            stats['matched']['failure_count'] = matched_failures
        else:
            for key in stats['matched']:
                stats['matched'][key] = sorted(stats['matched'][key])
            stats['matched']['failure_total'] = 0
            stats['matched']['failure_count'] = 0

        return JsonResponse(stats)

    def get(self, request, *args, **kwargs):
        action = kwargs.get('action')
        if action == "history":
            return self.__history__(request, kwargs['pk'])

        raise ValidationError("The action '{}' is unsupported".format(action))

    def post(self, request, *args, **kwargs):
        action = kwargs.get('action')
        if action == "stats":
            return self.__stats__(request)

        raise ValidationError("The action '{}' is unsupported".format(action))


class MachineView(HistoryView):
    def __history__(self, request, pk):
        machine = get_object_or_404(Machine, pk=pk)

        if machine.vetted_on is not None:
            vetted_on = "{} ({})".format(machine.vetted_on.date(), naturaltime(machine.vetted_on))
        else:
            vetted_on = "Not vetted yet"

        info = [
            ("Name", machine.name),
            ("Added on", "{} ({})".format(machine.added_on.date(), naturaltime(machine.added_on))),
            ("Public", machine.public),
            ("Vetted on", vetted_on),
        ]

        machine_tags = [render_to_string("CIResults/basic/machine_tag.html", {"machine_tag": m}).strip()
                        for m in machine.tags.all()]
        self.__add_info_line__(info, "Tags", machine_tags, "No tags yet", 1)

        # This machine may be the parent of other machines
        children = list(Machine.objects.filter(aliases=machine))
        if len(children) > 0:
            children_str = [render_to_string("CIResults/basic/machine.html", {"machine": m}).strip() for m in children]
            self.__add_info_line__(info, "Children", children_str, "No children", 1)

        # This machine may also have a parent
        if machine.aliases is not None:
            parent_machine = [render_to_string("CIResults/basic/machine.html", {"machine": machine.aliases}).strip()]
            self.__add_info_line__(info, "Parent", parent_machine, "No parent", 1)

        # Render the page
        return self.__failure_history_page__(request, machine, info,
                                             {"result__ts_run__machine__in": [machine] + children})

    def __vet__(self, request, params):
        machines = set()
        for param in params:
            try:
                machines.add(int(param))
            except ValueError:
                continue

        # Update all the vetted tag of all the machines asked
        Machine.objects.filter(pk__in=machines).update(vetted_on=timezone.now())

        return redirect('CIResults-index')

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])

    def post(self, request, *args, **kwargs):
        return self.__vet__(request, request.POST)


class TestView(HistoryView):
    def __history__(self, request, pk):
        test = get_object_or_404(Test, pk=pk)

        if test.vetted_on is not None:
            vetted_on = "{} ({})".format(test.vetted_on.date(), naturaltime(test.vetted_on))
        else:
            vetted_on = "Not vetted yet"

        info = [
            ("Name", test.name),
            ("From the Testsuite", mark_safe(render_to_string("CIResults/basic/testsuite.html",
                                                              {"testsuite": test.testsuite}))),
            ("Added on", "{} ({})".format(test.added_on.date(), naturaltime(test.added_on))),
            ("Public", test.public),
            ("Vetted on", vetted_on),
        ]

        # Render the page
        return self.__failure_history_page__(request, test, info, {"result__test": test})

    def __vet__(self, request, params):
        tests = set()
        for param in params:
            try:
                tests.add(int(param))
            except ValueError:
                continue

        # Update all the vetted tag of all the tests asked
        Test.objects.filter(pk__in=tests).update(vetted_on=timezone.now())

        return redirect('CIResults-index')

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])

    def post(self, request, *args, **kwargs):
        return self.__vet__(request, request.POST)


class TestSuiteView(HistoryView):
    def __history__(self, request, pk):
        testsuite = get_object_or_404(TestSuite, pk=pk)

        if testsuite.vetted_on is not None:
            vetted_on = "{} ({})".format(testsuite.vetted_on.date(), naturaltime(testsuite.vetted_on))
        else:
            vetted_on = "Not vetted yet"

        statuses = [render_to_string("CIResults/basic/textstatus.html", {"status": s}).strip()
                    for s in testsuite.acceptable_statuses.all()]
        info = [
            ("Description", testsuite.description),
            ("Public", testsuite.public),
            ("Vetted on", vetted_on),
        ]

        self.__add_info_URL__(info, "Project URL", testsuite.url, index=1)
        self.__add_info_line__(info, "Acceptable statuses", statuses, "<None>")

        # Render the page
        return self.__failure_history_page__(request, testsuite, info, {"result__test__testsuite": testsuite})

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])


class ComponentView(HistoryView):
    def __history__(self, request, pk):
        component = get_object_or_404(Component, pk=pk)

        info = [
            ("Description", component.description),
            ("Public", component.public),
        ]

        self.__add_info_URL__(info, "Project URL", component.url, index=1)

        # Render the page, without any history
        return self.__failure_history_page__(request, component, info, {"result__id": -1})

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])


class TextStatusView(HistoryView):
    def __history__(self, request, pk):
        status = get_object_or_404(TextStatus, pk=pk)

        info = [
            ("From the Testsuite", mark_safe(render_to_string("CIResults/basic/testsuite.html",
                                                              {"testsuite": status.testsuite}))),
        ]

        # Render the page
        return self.__failure_history_page__(request, status, info, {"result__status": status})

    def __vet__(self, request, params):
        statuses = set()
        for param in params:
            try:
                statuses.add(int(param))
            except ValueError:
                continue

        # Update all the vetted tag of all the statuses asked
        TextStatus.objects.filter(pk__in=statuses).update(vetted_on=timezone.now())

        return redirect('CIResults-index')

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])

    def post(self, request, *args, **kwargs):
        return self.__vet__(request, request.POST)


class REST_RunConfigViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = RunConfig.objects.all().order_by('-id')
    serializer_class = RunConfigSerializer

    @classmethod
    def known_failures_serialized(cls, runcfg):
        f = KnownFailure.objects.filter(result__ts_run__runconfig=runcfg)
        failures = f.prefetch_related('result__status', 'result__test', 'result__test__testsuite',
                                      'result__ts_run__machine', 'matched_ifa__issue__bugs',
                                      'matched_ifa__issue__bugs__tracker')
        return KnownIssuesSerializer(failures, read_only=True, many=True)

    def _get_runcfg(self, obj_id):
        try:
            return RunConfig.objects.get(pk=int(obj_id))
        except ValueError:
            pass

        return get_object_or_404(RunConfig, name=obj_id)

    @detail_route()
    def known_failures(self, request, pk=None):
        runcfg = self._get_runcfg(pk)
        return Response(self.known_failures_serialized(runcfg).data, status=status.HTTP_200_OK)

    @detail_route()
    def compare(self, request, pk=None):
        runcfg_from = self._get_runcfg(pk)
        runcfg_to = self._get_runcfg(request.GET.get('to'))
        no_compress = request.GET.get('no_compress') is not None

        diff = runcfg_from.compare(runcfg_to, no_compress=no_compress)
        if request.GET.get('summary') is None:
            serializer = RunConfigDiffSerializer(diff)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return HttpResponse(diff.text)


class RunConfigView(HistoryView):
    def __history__(self, request, pk):
        runcfg = get_object_or_404(RunConfig, pk=pk)

        tags = [render_to_string("CIResults/basic/tag.html", {"tag": t}).strip() for t in runcfg.tags.all()]
        builds = [render_to_string("CIResults/basic/build.html", {"build": b}).strip() for b in runcfg.builds.all()]
        info = [
            ("Name", runcfg.name),
            ("Added on", "{} ({})".format(runcfg.added_on.date(), naturaltime(runcfg.added_on))),
        ]
        self.__add_info_URL__(info, "Project URL", runcfg.url, index=1)
        self.__add_info_line__(info, "Tags", tags, "<None>")
        self.__add_info_line__(info, "Builds", builds, "<None>")
        self.__add_info_line__(info, "Environment", runcfg.environment)

        # Render the page
        return self.__failure_history_page__(request, runcfg, info, {"result__ts_run__runconfig": runcfg},
                                             chart_all_failures=True)

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])


class REST_ComponentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Component.objects.all().order_by('-id')
    serializer_class = ComponentSerializer


class REST_BuildViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Build.objects.all().order_by('-id')
    serializer_class = BuildSerializer


class REST_TestSet(viewsets.ReadOnlyModelViewSet):
    queryset = Test.objects.all().order_by('-id')
    serializer_class = TestSerializer
    filter_fields = ('id', 'testsuite', 'public', 'vetted_on', 'name')


class BuildView(HistoryView):
    def __history__(self, request, pk):
        build = get_object_or_404(Build, pk=pk)

        component = render_to_string("CIResults/basic/component.html", {"component": build.component}).strip()

        info = [
            ("Name", build.name),
            ("Component", mark_safe(component)),
            ("Added on", "{} ({})".format(build.added_on.date(), naturaltime(build.added_on))),
        ]

        parents = [render_to_string("CIResults/basic/build.html", {"build": b}).strip() for b in build.parents.all()]
        self.__add_info_line__(info, "Parents", parents, "<None>")

        info.extend([
            ("GIT version", build.version),
            ("GIT branch", build.branch),
            ("GIT repository", build.repo),
            ("Build parameters", build.parameters),
            ("Build logs", build.build_log),
        ])

        self.__add_info_URL__(info, "Commit URL", build.upstream_url, index=-2)

        # Render the page, without any history
        return self.__failure_history_page__(request, build, info, {"result__id": -1})

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])


class RunConfigTagView(HistoryView):
    def __history__(self, request, pk):
        tag = get_object_or_404(RunConfigTag, pk=pk)

        info = [
            ("Name", tag.name),
            ("Description", tag.description),
            ("Public", tag.public),
        ]
        self.__add_info_URL__(info, "External URL", tag.url, index=2)

        # Render the page, without any history
        return self.__failure_history_page__(request, tag, info, {"result__ts_run__runconfig__tags": tag})

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])


class TestResultView(HistoryView):
    def __history__(self, request, pk):
        result = get_object_or_404(TestResult, pk=pk)

        test = mark_safe(render_to_string("CIResults/basic/test.html", {"test": result.test}).rstrip())
        status = "{}{}".format(render_to_string("CIResults/basic/textstatus.html", {"status": result.status}).rstrip(),
                               " (Failure)" if result.is_failure else "")
        info = [
            ("Test", test),
            ("TestSuite Run", result.ts_run),
            ("Status", mark_safe(status)),
            ("Start time", result.start),
            ("Duration", result.duration),
            ("Command line", result.command),
            ("Stdout", result.stdout),
            ("Stderr", result.stderr),
            ("Dmesg", result.dmesg),
        ]
        self.__add_info_URL__(info, "External URL", result.url, index=3)

        # Render the page, without any history
        return self.__result_history_page__(request, result, info)

    def get(self, request, *args, **kwargs):
        # For now, directly call history
        return self.__history__(request, kwargs['pk'])


class TestEditView(UpdateView):
    model = Test
    fields = ['public', 'vetted_on']
    template_name = 'CIResults/test_edit.html'

    def get_success_url(self):
        messages.success(self.request, "The test {} has been edited".format(self.object))
        return reverse("CIResults-tests")


class MachineEditView(UpdateView):
    model = Machine
    fields = ['description', 'public', 'vetted_on', 'aliases', 'color_hex', 'tags']
    template_name = 'CIResults/machine_edit.html'

    def get_success_url(self):
        messages.success(self.request, "The machine {} has been edited".format(self.object))
        return reverse("CIResults-machines")


class TestMassRenameView(FormView):
    form_class = TestMassRenameForm
    template_name = 'CIResults/test_mass_rename.html'

    def get_success_url(self):
        return reverse("CIResults-tests")

    def form_valid(self, form):
        if not form.is_valid():
            return super().form_valid(form)

        if self.request.POST.get('action') == 'check':
            return render(self.request, self.template_name, {'form': form})
        else:
            form.do_renaming()
            messages.success(self.request,
                             "Renamed {} tests ('{}' -> '{}')".format(len(form.affected_tests),
                                                                      form.cleaned_data.get('substring_from'),
                                                                      form.cleaned_data.get('substring_to')))
            return super().form_valid(form)


class TestRenameView(UpdateView):
    model = Test
    fields = ['name']
    template_name = 'CIResults/test_rename.html'

    def form_valid(self, form):
        # We do not want to save any change, we just want to call the test's rename method
        self.object.rename(form.cleaned_data['name'])
        messages.success(self.request, "The test {} has been renamed to {}".format(self.object,
                                                                                   form.cleaned_data['name']))
        return HttpResponseRedirect(reverse("CIResults-tests"))


class filterView(DetailView):
    model = IssueFilter
    template_name = 'CIResults/filter.html'


class REST_BugTrackerAccountViewSet(viewsets.ModelViewSet):
    queryset = BugTrackerAccount.objects.all().order_by('-id')
    serializer_class = BugTrackerAccountSerializer

    http_method_names = ['get', 'patch']


def object_vet(request, **kwargs):
    klass = kwargs['klass']
    obj = get_object_or_404(klass, pk=kwargs['pk'])

    try:
        obj.vet()
        messages.success(request, "The object {} has been successfully vetted".format(obj))
    except ValueError:
        messages.error(request, "The object {} is already vetted".format(obj))

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def object_suppress(request, **kwargs):
    klass = kwargs['klass']
    obj = get_object_or_404(klass, pk=kwargs['pk'])

    try:
        obj.suppress()
        messages.success(request, "The object {} has been successfully suppressed".format(obj))
    except ValueError:
        messages.error(request, "The object {} is already suppressed".format(obj))

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class SimpleSearchableMixin:
    @property
    def query(self):
        return self.request.GET.get('q', '')

    @property
    def public(self):
        return self.kwargs.get('public', True)

    # TODO: Add a nice UI for this
    def get_paginate_by(self, queryset):
        return self.request.GET.get('page_size', self.paginate_by)

    def get_context_data(self):
        context = super().get_context_data()
        context['public'] = self.public
        context['search_query'] = self.query
        context['query_get_param'] = "q=" + self.query
        return context


class TestListView(SimpleSearchableMixin, ListView):
    model = Test
    paginate_by = 100

    def get_queryset(self):
        tests = super().get_queryset().order_by('testsuite__name', 'name').prefetch_related('testsuite',
                                                                                            'first_runconfig')
        if self.public:
            tests = tests.exclude(public=False)
        if len(self.query) > 0:
            tests = tests.filter(name__regex=self.query)
        return tests


class MachineListView(SimpleSearchableMixin, ListView):
    model = Machine
    paginate_by = 100

    def get_queryset(self):
        machines = super().get_queryset().order_by('name').prefetch_related('aliases')
        if self.public:
            machines = machines.exclude(public=False)
        if len(self.query) > 0:
            machines = machines.filter(name__regex=self.query)
        return machines


class UserFiltrableMixin:
    # TODO: Add a nice UI for this
    def get_paginate_by(self, queryset):
        return self.request.GET.get('page_size', self.paginate_by)

    def get_context_data(self):
        context = super().get_context_data()
        context['query_get_param'] = 'query=' + self.query.user_query
        context['query'] = self.query

        return context


class TestResultListView(UserFiltrableMixin, ListView):
    model = TestResult
    paginate_by = 100

    def get_queryset(self):
        requested_filters = self.request.GET.copy()
        self.query = self.model.from_user_filters(**requested_filters)
        if not self.query.is_valid:
            messages.error(self.request, "Filtering error: " + self.query.error)

        if not self.query.is_empty:
            query = self.query.objects
            query = query.filter(ts_run__runconfig__temporary=False)  # Do not show results from temp runs
            query = query.prefetch_related('test',
                                           'test__testsuite',
                                           'ts_run',
                                           'ts_run__machine',
                                           'ts_run__runconfig',
                                           'ts_run__testsuite',
                                           'status',
                                           'status__testsuite',
                                           'known_failures__matched_ifa__filter',
                                           'known_failures__matched_ifa__issue__bugs__tracker')
            return query.order_by('-ts_run__runconfig__added_on')
        else:
            return self.model.objects.none().order_by('id')

    def get_context_data(self):
        context = super().get_context_data()
        context['results'] = context.pop('object_list')

        context["statuses_chart"] = metrics_testresult_statuses_stats(context['results']).stats()
        context["machines_chart"] = metrics_testresult_machines_stats(context['results']).stats()
        context["tests_chart"] = metrics_testresult_tests_stats(context['results']).stats()
        context["issues_chart"] = metrics_testresult_issues_stats(context['results']).stats()

        return context


class KnownFailureListView(UserFiltrableMixin, ListView):
    model = KnownFailure
    paginate_by = 100

    def get_queryset(self):
        requested_filters = self.request.GET.copy()
        self.query = self.model.from_user_filters(**requested_filters)
        if not self.query.is_valid:
            messages.error(self.request, "Filtering error: " + self.query.error)

        if not self.query.is_empty:
            query = self.query.objects
            query = query.filter(result__ts_run__runconfig__temporary=False)  # Do not show results from temp runs
            query = query.prefetch_related('result__test',
                                           'result__test__testsuite',
                                           'result__ts_run',
                                           'result__ts_run__machine',
                                           'result__ts_run__runconfig',
                                           'result__ts_run__testsuite',
                                           'result__status',
                                           'result__status__testsuite',
                                           'matched_ifa__filter',
                                           'matched_ifa__issue__bugs__tracker')
            return query.order_by('-result__ts_run__runconfig__added_on')
        else:
            return self.model.objects.none().order_by('id')

    def get_context_data(self):
        context = super().get_context_data()
        context['failures'] = context.pop('object_list')

        context["statuses_chart"] = metrics_knownfailure_statuses_stats(context['failures']).stats()
        context["machines_chart"] = metrics_knownfailure_machines_stats(context['failures']).stats()
        context["tests_chart"] = metrics_knownfailure_tests_stats(context['failures']).stats()
        context["issues_chart"] = metrics_knownfailure_issues_stats(context['failures']).stats()

        return context
