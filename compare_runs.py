#!/usr/bin/env python3

import django
import argparse
import json
import sys

django.setup()

from CIResults.models import RunConfig  # noqa
from CIResults.serializers import RunConfigDiffSerializer  # noqa

def get_runconfig_or_die(name):
    try:
        return RunConfig.objects.get(name=name)
    except RunConfig.DoesNotExist:
        print("ERROR: The runconfig '{}' does not exist in the database".format(name))
        sys.exit(1)

# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("runcfg1", help="Name of the runconfig that will serve as the base of the comparison")
parser.add_argument("runcfg2", help="Name of the runconfig that will compared to runcfg1")

parser.add_argument("-s", "--summary", action="store_true",
                    help="Only print the summary of the change instead of the full json")
parser.add_argument("--no_compress", action="store_true",
                    help="Do not compress results in the summary")
parser.add_argument("-sf", "--summary_file", default=None,
                    help="Location of the file where to store the summary")
parser.add_argument("-r", "--result", action="store_true",
                    help="Only print the status of the difference (SUCCESS, WARNING or FAILURE)")
parser.add_argument("-rf", "--result_file", default=None,
                    help="Location of the file where to store the result")

args = parser.parse_args()

# Get the two runconfigs, then compare them
runcfg1 = get_runconfig_or_die(name=args.runcfg1)
runcfg2 = get_runconfig_or_die(name=args.runcfg2)
diff = runcfg1.compare(runcfg2, no_compress=args.no_compress)

# FIXME: Since we do not have an easy way to get rid of flip-floppers, let's
# consider warnings as a success for now!
status = diff.status
if status == "WARNING":
    status = "SUCCESS"

summary_file = open(args.summary_file, mode="w") if args.summary_file is not None else sys.stdout
result_file = open(args.result_file, mode="w") if args.result_file is not None else sys.stdout

if args.summary:
    print(diff.text, file=summary_file)
if args.result:
    print(status, file=result_file)

if not args.summary and not args.result:
    ser = RunConfigDiffSerializer()
    print(json.dumps(ser.to_representation(diff), sort_keys=True, indent=4))

sys.exit(0)
