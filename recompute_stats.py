#!/usr/bin/env python3

import django
import argparse
import time
import sys

django.setup()

from CIResults.models import Issue, IssueFilterAssociated

issues = Issue.objects.filter(archived_on=None)
ifas = IssueFilterAssociated.objects.filter(issue__in=issues)

msg = "Updating the statistics of {} issues, and {} filters associations"
print(msg.format(len(issues), len(ifas)))

start = time.time()
for i, filter in enumerate(ifas):
    print("Updating filter association {}/{}".format(i + 1, len(ifas)), end='\r')
    filter.update_statistics()
print("Filter association statistics updated in {:.2f} ms".format((time.time() - start) * 1000))

start = time.time()
for i, issue in enumerate(issues):
    print("Updating Issue {}/{}".format(i + 1, len(issues)), end='\r')
    issue.update_statistics()
print("Issue statistics updated in {:.2f} ms".format((time.time() - start) * 1000))
